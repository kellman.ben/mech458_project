#pragma once

#include <inttypes.h>
#include "util/cli_defs.h"

void uart_init(cli_t *cli_instance);
void uart_printf(const char* fmt, ...);
void uart_println(const char* msg);
void uart_print(const char* msg);
