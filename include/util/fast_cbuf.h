#pragma once

#ifdef __cplusplus
extern "C" {
#endif
#include <inttypes.h>
#include <stdbool.h>
#include <assert.h>

#define fast_cbuf_init(handle, size) \
do{                           \
  (handle)->read_idx = 0;       \
  (handle)->write_idx = 0;           \
  (handle)->size_mask = (size);  \
}                              \
while(0)

/**
 * @brief Checks if a circular buffer is full
 * @param handle
 */
#define fast_cbuf_full(handle) ((((handle)->write_idx + 1) & (handle)->size_mask) == ((handle)->read_idx))

#define fast_cbuf_empty(handle) ((handle)->write_idx == (handle)->read_idx)

#define fast_cbuf_size(handle) (((handle)->write_idx - (handle)->read_idx) & (handle->size_mask))
#define fast_cbuf_capacity(handle) ((handle)->size_mask)

#define fast_cbuf_push(handle, value)                 \
do {                                                  \
    assert(!fast_cbuf_full(handle));                  \
    (handle)->storage[(handle)->write_idx] = (value); \
    ++((handle)->write_idx);                          \
    (handle)->write_idx &= ((handle)->size_mask);     \
} while(0)

#define fast_cbuf_pop(handle)\
do{                    \
  assert(!fast_cbuf_empty(handle)); \
  ++(handle)->read_idx;                         \
  (handle)->read_idx &= (handle)->size_mask;                     \
}while(0)

#define fast_cbuf_clear(handle)\
do{                    \
  (handle)->read_idx = 0;                         \
  (handle)->write_idx = 0;                         \
}while(0)

#define fast_cbuf_at(handle, n) ((handle)->storage[((handle)->read_idx + (n)) & ((handle)->size_mask)])
#define fast_cbuf_front(handle) ((handle)->storage[((handle)->read_idx)])

#ifdef __cplusplus
}
#endif