#pragma once
#ifdef __cplusplus
extern "C" {
#endif
#include <inttypes.h>

/**
 * Classifies feature vectors using a one-vs-all classifier
 * @param num_features The size of the feature vector (including the homogeneous dimension)
 * @param num_classes The number of different classes
 * @param w The weights matrix in row major order, where each row contains the weight vectors for one class. ex:
 * [ w0' ]
 * [ w1' ]
 * [  .  ]
 * [  .  ]
 * [  .  ]
 * [ wn' ]
 * @param x The feature vector (an array of size 'num_features')
 * @return The classification index based on the row order of the weights matrix
 */
uint8_t lr_classify(uint8_t num_features, uint8_t num_classes, const int8_t *w, const int32_t *b, const uint16_t *x);

#ifdef __cplusplus
}
#endif