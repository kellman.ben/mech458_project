#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

#define INFINITE_WAIT_TIME_MS (UINT16_MAX)

typedef enum {
  STEPPER_DIR_STOP = 0,
  STEPPER_DIR_CW = 1,
  STEPPER_DIR_CCW = -1,
  STEPPER_DIR_NOP = 2,
} stepper_dir_t;

typedef enum {
  STEPPER_MODE_ACCELERATE,
  STEPPER_MODE_COAST,
  STEPPER_MODE_WAIT,
} stepper_mode_t;

typedef struct {
  const uint16_t* accel_table;
  const uint16_t* decel_table;
  uint8_t accel_table_length;
  uint8_t decel_table_length;
  uint8_t starting_speed_idx;
  uint16_t dir_change_delay_ms;
} stepper_profile_t;

typedef struct {
  uint8_t steps_remaining;
  const uint16_t* step_profile;
  stepper_mode_t mode;
  stepper_dir_t dir;
} stepper_cmd_t;

typedef struct {
  volatile uint8_t write_idx;
  volatile uint8_t read_idx;
  uint8_t size_mask;
  stepper_cmd_t storage[];
} stepper_cmd_queue_t;

typedef struct {
  uint8_t abs_position;
  uint16_t step_period;
  stepper_dir_t direction;
} stepper_state_t;

typedef struct {
  uint16_t initial_delay_ms;
  stepper_dir_t direction;
  uint8_t accel_steps;
  uint8_t coast_steps;
  uint8_t decel_steps;
  const uint16_t* accel_table;
  const uint16_t* decel_table;
  uint16_t coast_period;
  uint16_t duration_ms;
} trapezoidal_path_t;

typedef struct {
  uint8_t cw_bound;
  uint8_t ccw_bound;
} stepper_bounds_t;

typedef struct {
  uint8_t target_pos;
  stepper_bounds_t target_bounds;
  stepper_bounds_t initial_bounds;
  uint16_t initial_bounds_exit_ms;
} stepper_path_constraints_t;

typedef struct {
  uint16_t entry_time;
  uint16_t exit_time;
  uint8_t entry_idx;
  uint8_t entry_step;
  uint8_t exit_idx;
  uint8_t exit_step;
} bound_crossings_t;

/**
 * @brief Plots a minimum time stepper motor trajectory than ends at the target point
 * @param profile The acceleration profile to use
 * @param initial_state The initial state of the stepper motor
 * @param target The position to move the stepper motor to
 * @param path_out The output buffer, must be an array of size 2 or greater.
 *      Assumes a stop step will be executed when the path is completed,
 *      so number of steps in trapezoidal paths will be one less than the total number of steps
 * @return The number of trapezoidal segments in the path
 */
uint8_t stepper_plan_endpoint_path(const stepper_profile_t* profile,
                                   const stepper_state_t* initial_state,
                                   uint8_t target,
                                   trapezoidal_path_t path_out[2]);

/**
 * Pareses the next step of of a command queue.
 * Call repeatably to execute a command queue until STEPPER_DIR_STOP is returned.
 * @param cmd_queue The command queue to execute, will be updated to remove the step that has been executed.
 * @param[out] step_period Will be set to the delay for the next step
 * @return The direction of the next step
 */
stepper_dir_t stepper_pop_next_step(stepper_cmd_queue_t* cmd_queue, volatile uint16_t* step_period);

/**
 * Gets the state of the stepper motor time_ms in the future.
 * @param cmd_queue The command queue the stepper motor is currently using
 * @param[out] state Must initially be set to the stepper motor's current state. Will be updated to the stepper motor's
 * future state after function has been called.
 * @param time_ms Time to find the motor's state at
 */
void stepper_find_future_state(const stepper_cmd_queue_t* cmd_queue, stepper_state_t* state, uint16_t time_ms);

/**
 * Queues the appropriate commands to execute a trapezoidal path to
 * @param path The trapezoidal path
 * @param cmd_queue The command queue to add the commands to
 */
void stepper_queue_trajectory(const trapezoidal_path_t* path, stepper_cmd_queue_t* cmd_queue);
/**
 * Should be called before stepper_queue_trajectory, if stepper is stopped
 * @param direction The direction to start moving in
 * @param cmd_queue The command queue to add the command to
 */
void stepper_queue_start(stepper_dir_t direction, stepper_cmd_queue_t* cmd_queue);
void time_to_bounds(uint8_t n, trapezoidal_path_t* path,
                    uint8_t initial_pos,
                    const stepper_bounds_t* bounds,
                    bound_crossings_t* bounds_crossings);
#ifdef __cplusplus
}
#endif