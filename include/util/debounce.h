#pragma once
#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

typedef enum {
  BUTTON_HIGH = 0,
  BUTTON_LOW = 1,
  BUTTON_RISING = 2,
  BUTTON_FALLING = 3,
} button_state_t;

typedef struct {
  // Pointer to the PIN register
  const volatile uint8_t *pin;
  // The PIN index (0-7)
  uint8_t idx;
  // The don't care mask, only left most bits should be set
  // Ex. 0xf0 will only debounce over 4 cycles instead of 8 (0x00)
  uint8_t dc_mask;

  // Bit mask used to store previous states
  uint8_t debounce_state;
  // The previous debounced state
  button_state_t prev_state;
} debounced_button_t;

/**
 * Call periodically to debounce a button
 * @param button The button struct to debounce
 * @return The current button state
 */
button_state_t debounce_button(debounced_button_t *button);

#ifdef __cplusplus
}
#endif