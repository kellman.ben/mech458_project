#include "stepper_ctrl.h"
#include "pin_defs.h"
#include "util/fast_cbuf.h"
#include <util/stepper_util.h>
#include "sys_tick.h"
#include "device/uart.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/atomic.h>
#include <stdbool.h>

#ifndef F_CPU
#define F_CPU 8000000UL        //Frequency of uC - ATmega2560
#endif

#include <util/delay.h>
// todo: add def to switch stepper driver mode

#define STEPPER_PLOT_TIME_MS (5)
#define STEPPER_PORT_MSK (0x3f)
#define STEP_PERIOD_US (20000u)
#define STEPPER_PROVISIONAL_CMD_BUF_SIZE (15)
#define MAX_ACCEL_STEPS (64)
#define HOME_OFFSET (2)

typedef struct {
  uint16_t accel_table[MAX_ACCEL_STEPS];
  uint16_t decel_table[MAX_ACCEL_STEPS];
  uint8_t accel_table_len;
  uint8_t decel_table_len;
  uint16_t dir_change_delay_ms;
} stepper_profile_eeprom_t;

static const uint8_t step_table[] = {0b101110,
                                     0b101101,
                                     0b110101,
                                     0b110110};

static stepper_profile_eeprom_t EEMEM profile_eeprom;

static uint16_t accel_table[MAX_ACCEL_STEPS] = {20000, 15311, 12801, 11196, 10063, 9209, 8538, 7992, 7538, 7152, 6820,
                                                6529, 6272, 6043, 5837, 5651, 5481, 5325};
static uint16_t decel_table[MAX_ACCEL_STEPS] = {5325, 5481, 5651, 5837, 6043, 6272, 6529, 6820, 7152, 7538, 7992,
                                                8538, 9209, 10063, 11196, 12801, 15311, 20000};

static stepper_profile_t stepper_profile = {
    .accel_table = accel_table,
    .decel_table = decel_table,
    .accel_table_length = 18,
    .decel_table_length = 18,
    .starting_speed_idx = 0,
    .dir_change_delay_ms = 75,
};

static uint8_t curr_step = 0;
static volatile stepper_state_t stepper_current_state = {.abs_position=0, .step_period=0, .direction=STEPPER_DIR_STOP};
static stepper_state_t stepper_end_state = {.abs_position=0, .step_period=0, .direction=STEPPER_DIR_STOP};

static uint8_t provisional_cmd_queue_storage[sizeof(stepper_cmd_queue_t) +
    (STEPPER_PROVISIONAL_CMD_BUF_SIZE + 1)*sizeof(stepper_cmd_t)];

static stepper_cmd_queue_t* cmd_queue_handle = (void*) provisional_cmd_queue_storage;

/**
 * @brief Immediately performs a single clock wise step
 */
static void step_cw();

/**
 * @brief Immediately performs a single counter clock wise step
 */
static void step_ccw();

static void execute_step();
static void execute_path(uint8_t n,
                         const trapezoidal_path_t path[n],
                         const stepper_state_t* path_start_state);

void stepper_init() {
  sys_tick_init();
  load_stepper_config();
  STEPPER_DDR |= STEPPER_PORT_MSK;

  TCCR3B |= (1 << CS11); /* Set timer clock prescaler to 8 */
  TCCR3B |= 1 << WGM42;   /* Clear on compare match */
  OCR3A = STEP_PERIOD_US;
  TCNT3 = 0x0000;  /* Initialize count to 0*/
  TIFR3 |= (1 << OCF3A); /* Clear compare match flag */
  fast_cbuf_init(cmd_queue_handle, STEPPER_PROVISIONAL_CMD_BUF_SIZE);
  DDRD |= 0x09;
}

void stepper_home() {
  while (!(STEPPER_PIN & HALL_PIN_MSK)) {
    step_cw();
    delay_ms(20);
  }
  while (STEPPER_PIN & HALL_PIN_MSK) {
    step_cw();
    delay_ms(20);
  }

  for (int i = 0; i < 2; ++i) {
    step_cw();
  }

  stepper_current_state.abs_position = 0;
}

uint16_t stepper_move(uint8_t pos, const stepper_bounds_t* endpoint_bounds) {
  PORTB |= 1 << PB6;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    stepper_end_state = stepper_current_state;
  }
  stepper_find_future_state(cmd_queue_handle, &stepper_end_state, STEPPER_PLOT_TIME_MS);

  trapezoidal_path_t path_buffer[4];
  uint16_t ret;
  PORTB |= 1 << PB5;

  uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                         &stepper_end_state,
                                         pos,
                                         path_buffer);

  uart_printf("starting pos: %d\n", stepper_end_state.abs_position);
  for (int i = 0; i < n; ++i) {
    uart_printf("a: %d, d:%d, c:%d\n",
                path_buffer[i].accel_steps,
                path_buffer[i].decel_steps,
                path_buffer[i].coast_steps);
  }
  bound_crossings_t crossings;
  time_to_bounds(n, path_buffer, stepper_end_state.abs_position, endpoint_bounds, &crossings);
  ret = crossings.entry_time;
  PORTB &= ~(1 << PB5);
  execute_path(n, path_buffer, &stepper_end_state);
  PORTB &= ~(1 << PB6);
  return ret;
}

bool stepper_try_trajectory(uint8_t endpoint,
                            const stepper_bounds_t* waypoint_bounds,
                            const stepper_bounds_t* endpoint_bounds,
                            uint16_t latest_entry_ms,
                            uint16_t soonest_exit_ms,
                            uint16_t* time_to_target_bounds) {
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    stepper_end_state = stepper_current_state;
  }
  stepper_find_future_state(cmd_queue_handle, &stepper_end_state, STEPPER_PLOT_TIME_MS);

  trapezoidal_path_t path_buffer[2];
  uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                         &stepper_end_state,
                                         endpoint,
                                         path_buffer);
  bound_crossings_t crossings;
  time_to_bounds(n, path_buffer, stepper_end_state.abs_position, waypoint_bounds, &crossings);

  // Only apply the entry time constraint if the direction is changing (n > 1)
  if (crossings.exit_time >= soonest_exit_ms &&
      (crossings.entry_time <= latest_entry_ms || n<=1)) {
    // trajectory is valid
    time_to_bounds(n, path_buffer, stepper_end_state.abs_position, endpoint_bounds, &crossings);
    *time_to_target_bounds = crossings.entry_time;
    execute_path(n, path_buffer, &stepper_end_state);
    return true;
  }

  // trajectory is invalid
  return false;
}
void load_stepper_config() {
  eeprom_read_block(&stepper_profile.accel_table_length, &profile_eeprom.accel_table_len,
                    sizeof(profile_eeprom.accel_table_len));
  eeprom_read_block(&stepper_profile.decel_table_length, &profile_eeprom.decel_table_len,
                    sizeof(profile_eeprom.decel_table_len));
  eeprom_read_block(&stepper_profile.dir_change_delay_ms, &profile_eeprom.dir_change_delay_ms,
                    sizeof(profile_eeprom.dir_change_delay_ms));
  eeprom_read_block(accel_table, profile_eeprom.accel_table, sizeof(accel_table));
  eeprom_read_block(decel_table, profile_eeprom.decel_table, sizeof(accel_table));
}

void save_stepper_config() {
  eeprom_write_block(&stepper_profile.accel_table_length, &profile_eeprom.accel_table_len,
                     sizeof(profile_eeprom.accel_table_len));
  eeprom_write_block(&stepper_profile.decel_table_length, &profile_eeprom.decel_table_len,
                     sizeof(profile_eeprom.decel_table_len));
  eeprom_write_block(&stepper_profile.dir_change_delay_ms, &profile_eeprom.dir_change_delay_ms,
                     sizeof(profile_eeprom.dir_change_delay_ms));
  eeprom_write_block(accel_table, profile_eeprom.accel_table, sizeof(accel_table));
  eeprom_write_block(decel_table, profile_eeprom.decel_table, sizeof(accel_table));
}

void set_profile_len(char mode, uint8_t len) {
  if (mode=='a') {
    stepper_profile.accel_table_length = len;
  } else if (mode=='d') {
    stepper_profile.decel_table_length = len;
  }
}

void set_profile(char mode, uint8_t idx, uint16_t value) {
  uint16_t* profile;
  if (mode=='a') {
    profile = accel_table;
  } else if (mode=='d') {
    profile = decel_table;
  } else {
    return;
  }
  profile[idx] = value;
}
void stepper_set_dir_change_delay(uint16_t delay) {
  stepper_profile.dir_change_delay_ms = delay;
}

void step_cw() {
  if (curr_step==3) {
    curr_step = 0;
  } else {
    curr_step++;
  }

  if (stepper_current_state.abs_position==199) {
    stepper_current_state.abs_position = 0;
  } else {
    stepper_current_state.abs_position++;
  }
  PORTD |= 0x1;
  _delay_us(2);
  PORTD |= 0x8;
  _delay_us(2);
  PORTD &= ~0x8;
  STEPPER_PORT = step_table[curr_step];
}

void step_ccw() {
  if (curr_step==0) {
    curr_step = 3;
  } else {
    curr_step--;
  }

  if (stepper_current_state.abs_position==0) {
    stepper_current_state.abs_position = 199;
  } else {
    stepper_current_state.abs_position--;
  }
  PORTD &= ~0x1;
  _delay_us(2);
  PORTD |= 0x8;
  _delay_us(2);
  PORTD &= ~0x8;
  STEPPER_PORT = step_table[curr_step];
}

ISR(TIMER3_COMPA_vect) {
  execute_step();
}

void execute_step() {
  stepper_dir_t dir = stepper_pop_next_step(cmd_queue_handle, &OCR3A);
  stepper_current_state.step_period = OCR3A;

  switch (dir) {
    case STEPPER_DIR_CW:
      stepper_current_state.direction = STEPPER_DIR_CW;
      step_cw();
      break;
    case STEPPER_DIR_CCW:
      stepper_current_state.direction = STEPPER_DIR_CCW;
      step_ccw();
      break;
    case STEPPER_DIR_STOP:
      stepper_current_state.direction = STEPPER_DIR_STOP;
      TIMSK3 &= ~0x2;
      break;
    default:
      break;
  }
}

void execute_path(uint8_t n, const trapezoidal_path_t* path, const stepper_state_t* path_start_state) {
  if (n > 0) {
    // wait for the stepper to be in the correct state
    uint16_t start_time_ms = curr_tick();
    while ((path_start_state->abs_position!=stepper_end_state.abs_position) &&
        (curr_tick() - start_time_ms) < 1000) {}

    if (curr_tick() - start_time_ms >= 1000) {
      uart_printf("Timed out waiting for stepper to reach end-state\n");
    }

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
      fast_cbuf_clear(cmd_queue_handle);
      // queue commands
      stepper_queue_start(path[0].direction, cmd_queue_handle);
      for (uint8_t i = 0; i < n; ++i) {
        stepper_queue_trajectory(&path[i], cmd_queue_handle);
      }
    }
  }
  if ((TIMSK3 & 0x2)==0) {
    execute_step();
    // Enable timer interrupts to perform stepping
    TCNT3 = 0x0000;  /* Initialize count to 0*/
    TIFR3 |= (1 << OCF3A); /* Clear compare match flag */
    TIMSK3 |= 0x2; /* Enable output compare match interrupt */
  }
}
