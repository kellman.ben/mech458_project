#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "util/stepper_util.h"

/**
 * @brief Initializes the stepper motor driver. Must be called before any other stepper functions.
 *
 */
void stepper_init();

/**
 * @brief Homes the stepper motor. Blocks until homing is complete
 * @pre stepper_init has been called
 */
void stepper_home();

/**
 * @brief Starts moving the stepper motor to the specified position (Non-blocking).
 * @pre stepper_home has been called
 * @param pos The target position in steps between 0 and 200
 * @param bound_cw
 * @param bound_ccw
 * @return
 */
uint16_t stepper_move(uint8_t pos, const stepper_bounds_t* endpoint_bounds);

bool stepper_try_trajectory(uint8_t endpoint,
                            const stepper_bounds_t* waypoint_bounds,
                            const stepper_bounds_t* endpoint_bounds,
                            uint16_t latest_entry_ms,
                            uint16_t soonest_exit_ms,
                            uint16_t* time_to_target_bounds);

void load_stepper_config();
void save_stepper_config();

void set_profile_len(char mode, uint8_t len);
void set_profile(char mode, uint8_t idx, uint16_t value);
void stepper_set_dir_change_delay(uint16_t delay);
