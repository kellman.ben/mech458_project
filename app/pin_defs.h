#pragma once

#include <avr/io.h>

// todo: add compile time switch for different defs
#define STEPPER_IO_BANK C
#define HALL_PIN_MSK (1 << PA6)

#define MOTOR_IO_BANK H
#define MOTOR_IA_MSK (0x1 << 5)
#define MOTOR_IB_MSK (0x1 << 6)

#define OR_INT_ID 4
#define EX_INT_ID 5
#define ADC_MUX_PIN (0x1)


#define GLUE_HELPER(x, y) x##y
#define GLUE(x, y) GLUE_HELPER(x, y)


#define STEPPER_PORT GLUE(PORT, STEPPER_IO_BANK)
#define STEPPER_PIN GLUE(PIN, STEPPER_IO_BANK)
#define STEPPER_DDR GLUE(DDR, STEPPER_IO_BANK)

#define OR_INT (GLUE(INT, OR_INT_ID))
#if OR_INT_ID > INT3
#define OR_EICR (EICRB)
#else
#define OR_EICR (EICRA)
#endif
#define OR_ISC (0x3 << GLUE(GLUE(ISC, OR_INT_ID), 0))
#define OR_INT_vect GLUE(INT, GLUE(OR_INT_ID, _vect))


#define MOTOR_PORT GLUE(PORT, MOTOR_IO_BANK)
#define MOTOR_DDR GLUE(DDR, MOTOR_IO_BANK)

#define EX_INT (GLUE(INT, EX_INT_ID))
#if EX_INT_ID > INT3
#define EX_EICR (EICRB)
#else
#define EX_EICR (EICRA)
#endif
#define EX_ISC (0x1 << GLUE(GLUE(ISC, EX_INT_ID), 0))
#define EX_INT_vect GLUE(INT, GLUE(EX_INT_ID, _vect))
