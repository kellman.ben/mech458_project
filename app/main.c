/*
 * mech458_project.c
 *
 * Created: 2022-10-21 7:12:01 PM
 * Author : Ben
 */

#include <pt/pt.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/cli.h>
#include <device/uart.h>
#include "sys_tick.h"
#include <assert.h>

#include "daq.h"
#include "commands.h"
#include "belt_ctrl.h"
#include "stepper_ctrl.h"
#include "util/fast_cbuf.h"
#include "sm.h"

#define DEPOSIT_TIME_MS (300) // Should be approx min deposit time
#define EXIT_TIME_MS (350) // Should be approx max depsoit time
#define ENTRY_TIME_MS (0)

PT_THREAD(sorter(struct pt *pt));

static const uint8_t item_positions[][4] = {
    {100, 100, 95,  105},
    {0,   0,   5,   10},
    {55,  45,  50,  50},
    {145, 155, 150, 150},
};

static const stepper_bounds_t item_bounds[4] = {
    {85,  115},
    {185, 15},
    {35,  65},
    {135, 165},
};
struct pt sorterPt;

int main(void) {
  CLKPR = 0x80;
  CLKPR = 0x01;
  cli();
  uart_init(&cli_g);
  sys_tick_init();
  daq_init();
  belt_init();
  stepper_init();
  sm_init();
  sei();
  stepper_home();
  PT_INIT(&sorterPt);
  while (1) {
    sm_process();
    daq_process(true);
    cli_process(&cli_g);
    if (sm_state() != SM_IDLE) {
      sorter(&sorterPt);
    }
  }
}

PT_THREAD(sorter(struct pt *pt)) {
  static uint16_t counter_start_ms = 0;
  static uint16_t delay_time_ms;
  static uint8_t num_repeated_items;
  static uint8_t item;
  static uint8_t prev_item = 1;
  PT_BEGIN(pt);
        while (1) {
          // Wait for an item to be classified
          PT_YIELD_UNTIL(pt, !fast_cbuf_empty(items_buffer_handle));
          item = fast_cbuf_front(items_buffer_handle);
          assert(item < 4);

          // Continuously compute trjaecotries until one satisfies timing constraints
          while (true){
            uint8_t next_item = item;
            if (fast_cbuf_size(items_buffer_handle) > 1) {
              next_item = fast_cbuf_at(items_buffer_handle, 1);
            }

            uint16_t latest_entry = 0;
            uint16_t soonest_exit = 0;
            uint16_t elapsed_ms = (curr_tick() - counter_start_ms);
            if (elapsed_ms < EXIT_TIME_MS){
              soonest_exit = EXIT_TIME_MS - elapsed_ms;
            }
            if (elapsed_ms < ENTRY_TIME_MS){
              latest_entry = ENTRY_TIME_MS - elapsed_ms;
            }
            bool path_ok = stepper_try_trajectory(item_positions[item][next_item],
                                                  &item_bounds[prev_item],
                                                  &item_bounds[item],
                                                  latest_entry, soonest_exit,
                                                  &delay_time_ms);
            if (path_ok){
              break;
            }
            PT_YIELD(pt);
          }

          if (delay_time_ms == 0xffff) {
            uart_printf("Received invalid delay time from stepper_move\n");
            delay_time_ms = EXIT_TIME_MS;
          }
          // Check how long until the stepper will be in position
          if (delay_time_ms < DEPOSIT_TIME_MS) {
            delay_time_ms = 0;
          } else {
            delay_time_ms -= DEPOSIT_TIME_MS;
          }
          // Wait for the stepper to be in position
          counter_start_ms = curr_tick();
          PT_WAIT_UNTIL(pt, (curr_tick() - counter_start_ms) > delay_time_ms);

          // deposit the first item
          belt_deposit_item();
          PT_WAIT_UNTIL(pt, belt_items_past_exit());
          // Check for repeated items, so we can skip the deposit delay for each item
          uint8_t total_items = fast_cbuf_size(items_buffer_handle);
          num_repeated_items = 1;
          for (uint8_t i = 0; (i < total_items - 1) && (total_items > 1); ++i) {
            if (fast_cbuf_at(items_buffer_handle, i) == fast_cbuf_at(items_buffer_handle, i + 1)) {
              belt_deposit_item();
              num_repeated_items++;
            } else {
              break;
            }
          }
          // Wait for any additional items to exit the belt
          PT_WAIT_UNTIL(pt, belt_items_past_exit());
          // Start timer when last item is path the exit
          counter_start_ms = curr_tick();

          // Mark all items as deposited
          for (uint8_t i = 0; i < num_repeated_items; ++i) {
            log_item_deposited(item);
            fast_cbuf_pop(items_buffer_handle);
          }
          prev_item = item;
          // Wait for items to fall of off the belt
//          PT_WAIT_UNTIL(pt, (curr_tick() - counter_start_ms) > EXIT_TIME_MS);
        }
  PT_END(pt);
}

ISR(BADISR_vect) {
  uart_printf("Bad ISR!\n");
}
