#pragma once

#include <stdint.h>
#include <stdbool.h>



/**
 * Initialize the belt control driver
 */
void belt_init();

/**
 * Stops the belt from moving
 * @pre belt_init() has been called
 */
void belt_stop();

/**
 * Deposits the next item on the belt as soon as possible
 */
void belt_deposit_item();

void belt_start();
bool belt_stopped();

bool belt_items_past_exit();

/**
 * Sets the speed that the belt will move at
 * @pre belt_init() has been called
 * @param speed The new belt speed
 */
void belt_speed_set(uint8_t speed);

/**
 * Polls the EX sensor, should be called at ~1kHz
 */
void belt_poll_exit();

/**
 * Checks if the DC motor is running
 */
bool motor_running();

void load_belt_speed();
void save_belt_speed();