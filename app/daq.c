#include "daq.h"
#include "util/lr_classifier.h"
#include "util/fast_cbuf.h"
#include "pin_defs.h"
#include "sm.h"
#include "belt_ctrl.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>

#include <device/uart.h>
#include <assert.h>

/**
 * Pin mappings:
 *  PF0/A0      -> Reflectance Sensor
 *  PE4/D2/INT4 -> Optical R Sensor
 */

#define MIN_NUM_SAMPLES  (100u)
#define DEBOUNCE_SAMPLES (10u)
#define STATS_BUFFER_SIZE (15)

typedef struct{
  uint16_t min;
  uint16_t n;
  uint32_t sum;
  uint32_t sqr_sum;
}item_stats_t;

typedef struct{
  volatile uint8_t read_idx;
  volatile uint8_t write_idx;
  uint8_t size_mask;
  item_stats_t storage[STATS_BUFFER_SIZE+1];
}stats_buffer_t;

items_buf_handle_t items_buffer_handle;

static const char *class_names[] = {"WHT", "BLK", "ALM", "STL"};

static daq_config_t EEMEM nvm_config;
static daq_config_t ram_config;

/* Statistics */
static stats_buffer_t stats_buffer;
static stats_buffer_t* stats_buffer_handle = &stats_buffer;
static item_stats_t current_stats = {};

/* Flags */
static volatile bool daq_running_ = false;

/* Only used in the adc interrupt ISR to keep track of how long the OR pin has been low */
static uint8_t debounce_count_ = 0;
static items_buffer_t items_buffer;

static void daq_start();

void daq_init() {
  EIMSK |= 1 << OR_INT; /* Enable INT4 */
  OR_EICR |= OR_ISC; /* Rising edge triggered */

  ADCSRA = 1 << ADEN; /* Enable ADC */
  ADCSRA |= 0x6; /* Set pre-scalar to 64 (125kHz ADC clock)*/
  ADCSRA |= 1 << ADIE; /* enable interrupt of ADC */
  ADMUX = ADC_MUX_PIN | (1 << REFS0); /*AVCC with external capacitor at AREF pin*/
  items_buffer_handle = &items_buffer;
  fast_cbuf_init(items_buffer_handle, DAQ_BUFFER_SIZE);
  fast_cbuf_init(stats_buffer_handle, STATS_BUFFER_SIZE);
  DDRE &= ~(1 << PE4);
  daq_load_config();
}

void daq_start() {
  /* Don't do anything if the ADC is already sampling */
  if (daq_running_){
    return;
  }

  daq_running_ = true;
  /* Initialize statistics */
  current_stats.min = UINT16_MAX;
  current_stats.sqr_sum = 0;
  current_stats.n = 0;
  current_stats.sum = 0;

  /* Trigger first ADC conversion */
  ADCSRA |= (1 << ADSC);
}

void daq_process(bool log_result) {
  /* Wait for the ADC to acquire data for */

  if (!fast_cbuf_empty(stats_buffer_handle)){
    item_stats_t stats = fast_cbuf_front(stats_buffer_handle);
    fast_cbuf_pop(stats_buffer_handle);

    if (stats.n > MIN_NUM_SAMPLES){
      uint16_t mean = stats.sum / stats.n;

      uint8_t class= NULL_ITEM_CODE;

      if (stats.min > ram_config.white_thresh) {
        class = BLACK_ITEM_CODE;
      } else if (stats.min > ram_config.steel_thresh) {
        class = WHITE_ITEM_CODE;
      } else if (stats.min > ram_config.alumn_thresh) {
        class = STEEL_ITEM_CODE;
      } else {
        class = ALUMN_ITEM_CODE;
      }

      log_item_classified(class);
      assert(!fast_cbuf_full(items_buffer_handle));
      fast_cbuf_push(items_buffer_handle, class);

      if (log_result) {
        uart_printf("min:%4u, mean:%4u, count:%5u, class: %s\n", stats.min, mean, stats.n, class_names[class]);
      }
    } else if (log_result) {
      uart_printf("Discard reading with: %d\n", stats.n);
    }
  }
}

void daq_load_config() {
  eeprom_read_block(&ram_config, &nvm_config, sizeof(daq_config_t));
}

void daq_save_config() {
  eeprom_write_block(&ram_config, &nvm_config, sizeof(daq_config_t));
}

void daq_update_config(const daq_config_t *config) {
  ram_config = *config;
}

const daq_config_t *daq_read_config() {
  return &ram_config;
}

ISR(ADC_vect) {
  /* Debounce to ignore any quick drops to low */
  if (PINE & (1 << PE4)){
    debounce_count_ = 0;
  } else{
    ++debounce_count_;
  }

  /* Read the sample value from the ADC */
  uint8_t low = ADCL;
  uint8_t high = ADCH;
  uint16_t current_sample = ((uint16_t)high << 8) | (uint16_t)low;

  if (motor_running()) {
    /* Update statistics */
    current_stats.sum += current_sample;
    ++current_stats.n;
  }

  if (current_sample < current_stats.min) {
    current_stats.min = current_sample;
  }

  /* Check if sampling should continue */
  if (debounce_count_ < DEBOUNCE_SAMPLES) {
    ADCSRA |= (1 << ADSC);
  } else {
    daq_running_ = false;
    assert(!fast_cbuf_full(stats_buffer_handle));
    fast_cbuf_push(stats_buffer_handle, current_stats);
    current_stats.min = UINT16_MAX;
    current_stats.sqr_sum = 0;
    current_stats.n = 0;
    current_stats.sum = 0;
  }
}

ISR(OR_INT_vect) {
  daq_start();
}
