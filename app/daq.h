/**
 * daq.h
 *
 * The data acquisition module, responsible for measuring the reflectivity of objects on the belt.
 */
#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

#define DAQ_BUFFER_SIZE (63u)

#define WHITE_ITEM_CODE (0u)
#define BLACK_ITEM_CODE (1u)
#define ALUMN_ITEM_CODE (2u)
#define STEEL_ITEM_CODE (3u)
#define NULL_ITEM_CODE (4u)

static_assert((DAQ_BUFFER_SIZE & (DAQ_BUFFER_SIZE + 1)) == 0, "Buffer size + 1 must be a power of 2");

typedef struct{
  volatile uint8_t write_idx;
  volatile uint8_t read_idx;
  uint8_t size_mask;
  uint8_t storage[DAQ_BUFFER_SIZE+1];
}items_buffer_t;

typedef items_buffer_t* items_buf_handle_t;

extern items_buf_handle_t items_buffer_handle;

typedef struct {
  uint16_t white_thresh;
  uint16_t steel_thresh;
  uint16_t alumn_thresh;
} daq_config_t;

/**
 * Initializes the data acquisition module. Should be called once at the start of the code
 */
void daq_init();

/**
 * Processes adc data. Should be called frequently in the main loop.
 */
void daq_process(bool log_result);

void daq_load_config();
void daq_save_config();
void daq_update_config(const daq_config_t* config);
const daq_config_t* daq_read_config();
