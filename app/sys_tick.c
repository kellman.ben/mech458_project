#include "sys_tick.h"
#include "belt_ctrl.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <util/atomic.h>

static volatile uint16_t tick_ = 0;
static bool sys_tick_inited_ = false;

void sys_tick_init() {
  if (!sys_tick_inited_) {
    TCCR1B |= (1 << CS11); /* Set timer clock prescaler to 8 */

    TCCR1B |= 1 << WGM12;     /* Clear on compare match */
    OCR1A = 0x03e8;     /* Set compare to 1000 counts */
    TCNT1 = 0x0000;    /* Initialize count to 0*/
    TIFR1 |= (1 << OCF1A); /* Clear compare match flag */
    TIMSK1 |= 0x2; /* Enable output compare match interrupt */
    sys_tick_inited_ = true;
  }

}

uint16_t curr_tick() {
  uint16_t cur_tick;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    cur_tick = tick_;
  }
  return cur_tick;
}
void delay_ms(uint16_t delay_ms) {
  uint16_t start_time = curr_tick();
  while ((curr_tick() - start_time) < delay_ms) {}
}

ISR(TIMER1_COMPA_vect) {
  TIFR1 |= (1 << OCF1A); /* Clear compare match flag */
  tick_++;
  belt_poll_exit();
}