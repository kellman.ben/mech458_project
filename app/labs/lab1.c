/* ##################################################################
# MILESTONE: 1
# PROGRAM: 1
# PROJECT: Lab1 Demo
# GROUP: 4, Lab B04
# NAME 1: Joshua, Fernandes, V00921078
# NAME 2: Ben, Kellman, V00898289
# DESC: This program performs the nightrider  sequence on a set of LEDs. 
# DATE: Spetember 23, 2022
# REVISED ############################################################### */

#include <stdlib.h> // the header of the general-purpose standard library of C programming language
#include <avr/io.h>// the header of I/O port
#include <util/delay_basic.h> // the header of delays

void delaynus(int n);
void delaynms(int n);

#define DELAY (200u)

/* ################## MAIN ROUTINE ################## */
int main(int argc, char *argv[]){
	DDRL = 0xFF; // Sets all pins on PORTL to output
	DDRC = 0xFF; 
	PORTL = 0xF0; // initialize pins to high to turn on LEDs (1 Yel & 1 Grn)
	while(1){
		uint8_t LED = 0x0F;
		
		for(int i = 3; i>0; i--){
			PORTC = LED >> i;
			delaynms(DELAY);
		}
		
		for(int i = 0; i<8; i++){
			PORTC = LED << i;
			delaynms(DELAY);
		}
		
		for(int i = 7; i>0; i--){
			PORTC = LED << i;
			delaynms(DELAY);
		}
		
		for(int i = 0; i<2; i++){
			PORTC = LED >> i;
			delaynms(DELAY);
		}
	};
	// generally means no error was returned
}

void delaynus(int n){
	for(int k = 0; k < n ; k++)
		_delay_loop_1(1);	
}

void delaynms(int n){
	for(int k = 0; k < n; k++)
		delaynus(1000);
}
