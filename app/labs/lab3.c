/* Solution Set for the LinkedQueue.c */
/* 	
	Course		: UVic Mechatronics 458
	Milestone	: 3
	Title		: Data structures for MCUs and the Linked Queue Library

	Name 1:					Student ID:
	Name 2:					Student ID:
	
	Description: You can change the following after you read it.  Lab3 Demo
	
	This main routine will only serve as a testing routine for now. At some point you can comment out
	The main routine, and can use the following library of functions in your other applications

	To do this...make sure both the .C file and the .H file are in the same directory as the .C file
	with the MAIN routine (this will make it more convenient)
*/

/* include libraries */
#include <stdlib.h>
#include <avr/io.h>
#include "lab3.h"
/* This is the attached header file, which cleans things up */						
/* Make sure you read it!!! */
/* global variables */
/* Avoid using these */

/* main routine 
   You need to add the mtimer function to this project.    */

void mTimer(int count){
	int i = 0;
	TCCR1B |= _BV(WGM12);
	OCR1A = 0x03E8;//output compare reg value

	//0b 0000 0011 1110 1000; dec: 1000 = 1ms

	TCNT1 = 0x0000;//timer/counter init value
	TIMSK1 = TIMSK1 | 0b00000010;//enable output compare interrupt
	TIFR1 |= _BV(OCF1A);//timer/counter interrupt flag register
	//
	while(i<count){
		if((TIFR1 & 0x02) == 0x02){
			TIFR1 |= _BV(OCF1A);
			i++;
		}
	}
	return;
}

void wait_for_press_and_release();

void wait_for_press_and_release(){
	/* Wait while pin is NOT pressed (Logic HIGH)*/
	while ((PINA & (1 << PA2)) == (1 << PA2)){}
	/* Pin is now pressed, wait 30ms to debounce*/
	mTimer(30);
	/*Wait while pin is pressed (Logic LOW)*/
	while ((PINA & (1 << PA2)) == 0){}
	/* Pin is now released, wait 30ms to debounce*/
	mTimer(20);
}

int main(){	

	link *head;			/* The ptr to the head of the queue */
	link *tail;			/* The ptr to the tail of the queue */
	link *newLink;		/* A ptr to a link aggregate data type (struct) */
	link *rtnLink;		/* same as the above */
	element eTest;		/* A variable to hold the aggregate data type known as element */

	CLKPR = 0x80;
	CLKPR = 0x01;
	TCCR1B |= _BV(CS11);

	DDRC = 0xFF; 		/* Used for debugging purposes only LEDs on PORTC */
	DDRA = 0x00;
	

	rtnLink = NULL;
	newLink = NULL;

	setup(&head, &tail);
	
	while (1)
	{
		PORTC = 0;
		/* Get user input (4 items) */
		for (uint8_t i=0; i<4; i++)
		{
			wait_for_press_and_release();
			/* Read data */
			uint8_t data = PINA & 0x3;
			
			/*Push to back of queue*/
			initLink(&newLink);
			newLink->e.itemCode = data;
			enqueue(&head, &tail, &newLink);
		}
		
		/* Pop item from front of queue */
		dequeue(&head, &rtnLink);
		free(rtnLink);
		
		for (uint8_t i=0; i<3; i++)
		{
			/*Pop remaining items from front of queue and display*/
			dequeue(&head, &rtnLink);
			PORTC |= (rtnLink->e.itemCode) << (2*i);
			free(rtnLink);
			mTimer(2000);
		}
		clearQueue(&head, &tail);
		
		/* Reset when the user presses the button */
		wait_for_press_and_release();
	}
	return(0);
}/* main */


/**************************************************************************************/
/***************************** SUBROUTINES ********************************************/
/**************************************************************************************/







/**************************************************************************************
* DESC: initializes the linked queue to 'NULL' status
* INPUT: the head and tail pointers by reference
*/

void setup(link **h,link **t){
	*h = NULL;		/* Point the head to NOTHING (NULL) */
	*t = NULL;		/* Point the tail to NOTHING (NULL) */
	return;
}/*setup*/




/**************************************************************************************
* DESC: This initializes a link and returns the pointer to the new link or NULL if error 
* INPUT: the head and tail pointers by reference
*/
void initLink(link **newLink){
	//link *l;
	*newLink = malloc(sizeof(link));
	(*newLink)->next = NULL;
	return;
}/*initLink*/




/****************************************************************************************
*  DESC: Accepts as input a new link by reference, and assigns the head and tail		
*  of the queue accordingly				
*  INPUT: the head and tail pointers, and a pointer to the new link that was created 
*/
/* will put an item at the tail of the queue */
void enqueue(link **h, link **t, link **nL){

	if (*t != NULL){
		/* Not an empty queue */
		(*t)->next = *nL;
		*t = *nL; //(*t)->next;
	}/*if*/
	else{
		/* It's an empty Queue */
		//(*h)->next = *nL;
		//should be this
		*h = *nL;
		*t = *nL;
	}/* else */
	return;
}/*enqueue*/




/**************************************************************************************
* DESC : Removes the link from the head of the list and assigns it to deQueuedLink
* INPUT: The head and tail pointers, and a ptr 'deQueuedLink' 
* 		 which the removed link will be assigned to
*/
/* This will remove the link and element within the link from the head of the queue */
void dequeue(link **h, link **deQueuedLink){
	/* ENTER YOUR CODE HERE */
	*deQueuedLink = *h;	// Will set to NULL if Head points to NULL
	/* Ensure it is not an empty queue */
	if (*h != NULL){
		*h = (*h)->next;
	}/*if*/
	
	return;
}/*dequeue*/




/**************************************************************************************
* DESC: Peeks at the first element in the list
* INPUT: The head pointer
* RETURNS: The element contained within the queue
*/
/* This simply allows you to peek at the head element of the queue and returns a NULL pointer if empty */
element firstValue(link **h){
	return((*h)->e);
}/*firstValue*/





/**************************************************************************************
* DESC: deallocates (frees) all the memory consumed by the Queue
* INPUT: the pointers to the head and the tail
*/
/* This clears the queue */
void clearQueue(link **h, link **t){

	link *temp;

	while (*h != NULL){
		temp = *h;
		*h=(*h)->next;
		free(temp);
	}/*while*/
	
	/* Last but not least set the tail to NULL */
	*t = NULL;		

	return;
}/*clearQueue*/





/**************************************************************************************
* DESC: Checks to see whether the queue is empty or not
* INPUT: The head pointer
* RETURNS: 1:if the queue is empty, and 0:if the queue is NOT empty
*/
/* Check to see if the queue is empty */
char isEmpty(link **h){
	/* ENTER YOUR CODE HERE */
	return(*h == NULL);
}/*isEmpty*/





/**************************************************************************************
* DESC: Obtains the number of links in the queue
* INPUT: The head and tail pointer
* RETURNS: An integer with the number of links in the queue
*/
/* returns the size of the queue*/
int size(link **h, link **t){

	link 	*temp;			/* will store the link while traversing the queue */
	int 	numElements;

	numElements = 0;

	temp = *h;			/* point to the first item in the list */

	while(temp != NULL){
		numElements++;
		temp = temp->next;
	}/*while*/
	
	return(numElements);
}/*size*/

