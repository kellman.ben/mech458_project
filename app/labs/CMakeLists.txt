add_avr_executable(lab1 lab1.c)
target_compile_options(lab1.elf PRIVATE -Wall)

add_avr_executable(lab2 lab2.c)
target_compile_options(lab2.elf PRIVATE -Wall)

add_avr_executable(lab3 lab3.c)
target_compile_options(lab3.elf PRIVATE -Wall)

add_avr_executable(lab4a lab4a.c)
target_compile_options(lab4a.elf PRIVATE -Wall)