/*
 * Lab4A.c
 *
 * Created: 2022-10-21 2:32:26 PM
 * Author : mech458
 */

#include <avr/io.h>

#define STEP_DELAY (20)

const uint8_t step_table[] = {0b110000, 0b000110, 0b101000, 0b000101};
volatile uint8_t curr_step = 0;

void mTimer(int count){
  int i = 0;
  TCCR1B |= _BV(WGM12);
  OCR1A = 0x03E8;//output compare reg value

  //0b 0000 0011 1110 1000; dec: 1000 = 1ms

  TCNT1 = 0x0000;//timer/counter init value
  TIMSK1 = TIMSK1 | 0b00000010;//enable output compare interrupt
  TIFR1 |= _BV(OCF1A);//timer/counter interrupt flag register
  //
  while(i<count){
    if((TIFR1 & 0x02) == 0x02){
      TIFR1 |= _BV(OCF1A);
      i++;
    }
  }
  return;
}

void step_cw(uint8_t steps){
  for(uint8_t i=0; i<steps; i++){
    curr_step++;
    if (curr_step >= 4)
    {
      curr_step = 0;
    }
    PORTA = step_table[curr_step];
    mTimer(STEP_DELAY);
  }
}

void step_ccw(uint8_t steps){
  for(uint8_t i=0; i<steps; i++){
    if (curr_step == 0){
      curr_step = 3;
    }
    else{
      curr_step--;
    }
    PORTA = step_table[curr_step];
    mTimer(STEP_DELAY);
  }
}

void home(){
  while (PINA & (1<<6))
  {
    step_cw(1);
  }
}


int main(void)
{
  CLKPR = 0x80;
  CLKPR = 0x01;
  TCCR1B |= _BV(CS11);

  /* Set timer 0 to fast PWM (WGM=3) */
  TCCR0A = 0x3;

  /* Enable output compare match a interrupt 	*/
  TIMSK0 = 0x2;

  /* Clear on compare match COM=3 */
  TCCR0A |= 2 << 6;

  /* Set clock prescaler to 8*/
  TCCR0B = 0x2;

  /* Set to 50% duty cycle */
  OCR0A = 128;

  /* Set pin B7 to output */
  DDRB |= 1 << 7;

  DDRA = 0xff;
  DDRC = 0xff;
  DDRA &= ~(1 << 6);
  /* Replace with your application code */


  home();
  mTimer(5000);

  step_cw(17);
  mTimer(2000);
  step_cw(33);
  mTimer(2000);
  step_cw(100);

  mTimer(2000);

  step_ccw(17);
  mTimer(2000);
  step_ccw(33);
  mTimer(2000);
  step_ccw(100);


  while (1)
  {
  }
}

