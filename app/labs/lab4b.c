#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#include "lcd.h"
// define the global variables that can be used in every function ==========
volatile unsigned char ADC_result;
volatile unsigned int ADC_result_flag;


void mTimer(uint16_t delay_ms){
	
	/* Configure Timer */
	
	TCCR1B |= 1 << WGM12;	 /* Clear on compare match */
	OCR1A = 0x03e8;	 /* Set compare to 1000 counts */
	TCNT1 = 0x0000;	/* Initialize count to 0*/
	TIFR1 |= (1 << OCF1A); /* Clear compare match flag */
	
	for (uint16_t i=0; i<delay_ms; i++)
	{
		while((TIFR1 & 0x02) == 0){} /* Wait for compare match flag to be set */
		TIFR1 |= (1 << OCF1A); /* Clear compare match flag */
	}
}


int main()
{	
	CLKPR = 0x80;
	CLKPR = 0x01;
	TCCR1B |= (1 << CS11); /* Set timer clock prescaler to 8 */
	
	// Make sure the motor is stopped
	PORTA |= 0x3;
	// Set PA0 & PA1 to output
	DDRA |= 0x3;
	
	/* Set timer 0 to fast PWM (WGM=3) */
	TCCR0A = 0x3;
		
	/* Clear on compare match COM=3 */
	TCCR0A |= 2 << 6;
		
	/* Set clock prescaler to 8*/
	TCCR0B = 0x2;
		
	/* Set pin B7 to output */
	DDRB = 0x0;
	DDRB |= 1 << 7;
	
	cli(); // disable all of the interrupt ==========================
	//Initialize LCD module
	InitLCD(0);

	//Clear the screen
	LCDClear();
	
	// config the external interrupt ======================================
	EIMSK |= 1; // enable INT0
	EICRA |= 0x3; // rising edge interrupt
	
	// config ADC =========================================================
	// by default, the ADC input (analog input is set to be ADC0 / PORTF0
	ADCSRA |= _BV(ADEN); // enable ADC
	ADCSRA |= _BV(ADIE); // enable interrupt of ADC
	// Left justify adc result (ie. 8 most significant bits are in the ADCH register and 2 lsb are in the ADCL register)
	ADMUX |= _BV(ADLAR) | _BV(REFS0);
	// set the PORTC as output to display the ADC result ==================
	DDRC = 0xff;
	// sets the Global Enable for all interrupts ==========================
	sei();
	// initialize the ADC, start one conversion at the beginning ==========
	ADCSRA |= _BV(ADSC);
	char line2[16];
	
	mTimer(5000);
	
	uint8_t prev_pin_state = 0xff;
	uint8_t pin_a_reading=0;
	while (1)
	{
		pin_a_reading = PINA & (1 << 2);
		// Check if pin state has changed
		if (pin_a_reading != prev_pin_state)
		{
			prev_pin_state = pin_a_reading;
			// Stop Motor (PA1 & PA0 high)
			PORTA |= 0x3;
			mTimer(5);
			if (prev_pin_state)
			{
				
				LCDWriteStringXY(0, 0, "REV");
				// Set PA1 to low
				PORTA &= ~(1 << 1);
			}
			else{
				LCDWriteStringXY(0, 0, "FWD");
				// Set PA0 to low
				PORTA &= ~(1 << 0);
			}
		}
		
		// Update LCD with new adc result
		if (ADC_result_flag)
		{
			// PORTC = ADC_result;
			ADC_result_flag = 0x00;
			OCR0A = ADC_result;
			ADCSRA |= _BV(ADSC);
			snprintf(line2, 16, "Speed: %03d%%", ((uint16_t)ADC_result*100)/255);
			LCDWriteStringXY(0, 1, line2);
		}
	}
} // end main


// sensor switch: Active HIGH starts AD converstion =======
ISR(INT0_vect)
{ 
	// Kill switch isr
	// Stop the motor, disable interrupts and wait forever to ensure nothing else can happen
	// until the mcu is reset
	PORTA |= 0x3;
	cli();
	while(1){}
}

// the interrupt will be trigured if the ADC is done ========================
ISR(ADC_vect)
{
	ADC_result = ADCH;
	ADC_result_flag = 1;
}