/*
 * Lab2.c
 *
 * Created: 2022-10-07 2:28:56 PM
 * Author : mech458
 */ 

#include <avr/io.h>

void mTimer(uint16_t delay_ms);

int main(void)
{
	
	CLKPR = 0x80;
	CLKPR = 0x01;
	TCCR1B |= (1 << CS11); /* Set timer clock prescaler to 8 */
	
	DDRL = 0xff; /* Set all pings on port L to output */
	DDRC = 0xff;
	
    /* Replace with your application code */
    while (1) 
    {
		for (uint8_t i=0; i<7; i++)
		{
			PORTC = 0x3 << i;
			mTimer(1000);
		}
		
		for (uint8_t i=5; i!=0; i--)
		{
			PORTC = 0x3 << i;
			mTimer(1000);
		}
    }
}


void mTimer(uint16_t delay_ms){
	
	/* Configure Timer */
	
	TCCR1B |= 1 << WGM12;	 /* Clear on compare match */
	OCR1A = 0x03e8;	 /* Set compare to 1000 counts */
	TCNT1 = 0x0000;	/* Initialize count to 0*/
	TIMSK1 |= 0x2; /* Enable output compare match flag */
	TIFR1 |= (1 << OCF1A); /* Clear compare match flag */
	
	for (uint16_t i=0; i<delay_ms; i++)
	{
		while((TIFR1 & 0x02) == 0){} /* Wait for compare match flag to be set */
		TIFR1 |= (1 << OCF1A); /* Clear compare match flag */
	}
}