#pragma once
#include <stdint.h>

/**
 * Initializes the 'sys_tick' timer
 */
void sys_tick_init();

/**
 *  The current tick will monotonically increase by 1 ever millisecond
 * @pre sys_tick_init() must have been called
 * @return The current tick value
 */
uint16_t curr_tick();

void delay_ms(uint16_t delay_ms);