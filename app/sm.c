#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include "sm.h"
#include "sys_tick.h"
#include "belt_ctrl.h"
#include "util/fast_cbuf.h"
#include "daq.h"
#include "device/uart.h"
#include "device/lcd.h"
#include "util/debounce.h"

#define SM_UPDATE_PERIOD_MS (4)
#define BELT_EMPTY_TIME_MS (3500)

#define BUTTON_DDR (DDRD)
#define BUTTON_PIN (PIND)
#define BUTTON_PAUSE_IDX (PD1)
#define BUTTON_PAUSE_MSK (1 << BUTTON_PAUSE_IDX)
#define BUTTON_RAMP_IDX (PD2)
#define BUTTON_RAMP_MSK (1 << BUTTON_RAMP_IDX)

typedef struct {
  uint8_t black;
  uint8_t white;
  uint8_t steel;
  uint8_t alumn;
} item_stats_t;

static sm_state_t current_state = SM_IDLE;
static uint16_t last_item_tick = 0;
static uint16_t last_run_time = 0;
static item_stats_t classified_items = {.black = 0, .white=0, .steel=0, .alumn=0};
static item_stats_t deposited_items = {.black = 0, .white=0, .steel=0, .alumn=0};

static uint16_t first_classified_ms = 0;
static uint16_t last_deposited_ms = 0;

debounced_button_t pause_button = {
    .prev_state = BUTTON_HIGH,
    .debounce_state = 0xff,
    .dc_mask = 0xf0,
    .pin = &BUTTON_PIN,
    .idx = BUTTON_PAUSE_IDX,
};

debounced_button_t ramp_button = {
    .prev_state = BUTTON_HIGH,
    .debounce_state = 0xff,
    .dc_mask = 0xf0,
    .pin = &BUTTON_PIN,
    .idx = BUTTON_RAMP_IDX,
};

const char *names[] = {"IDLE", "RUN", "RAMP"};
const char **sm_state_name = names;

static void display_stats();

void sm_process() {
  if ((curr_tick() - last_run_time) > SM_UPDATE_PERIOD_MS) {
    last_run_time = curr_tick();
    button_state_t pause_state = debounce_button(&pause_button);
    button_state_t ramp_state = debounce_button(&ramp_button);

    switch (current_state) {
      case SM_IDLE:
        if (pause_state == BUTTON_FALLING) {
          first_classified_ms = 0;
          last_deposited_ms = 0;
          current_state = SM_RUN;
          LCDClear();
          LCDWriteStringXY(0, 0, "RUN");
          belt_start();
        }
        break;
      case SM_RUN:
        if (pause_state == BUTTON_FALLING) {
          current_state = SM_IDLE;
          belt_stop();
          display_stats();
        } else if (ramp_state == BUTTON_FALLING) {
          LCDClear();
          LCDWriteStringXY(0, 0, "RAMP");
          current_state = SM_RAMP;
          last_item_tick = curr_tick();
        }
        break;
      case SM_RAMP:
        if (!fast_cbuf_empty(items_buffer_handle)) {
          last_item_tick = curr_tick();
        }
        if ((curr_tick() - last_item_tick) > BELT_EMPTY_TIME_MS) {
          current_state = SM_STOPPED;
          belt_stop();
          display_stats();
          cli();
        }
        break;
      case SM_STOPPED:
        while (1){}
      default:
        break;
    }
  }
}

sm_state_t sm_state() {
  return current_state;
}
void sm_init() {
  BUTTON_DDR &= ~(BUTTON_PAUSE_MSK | BUTTON_RAMP_MSK);
  InitLCD(0);
  LCDClear();
  LCDWriteStringXY(0, 0, "IDLE");
}

void log_item_classified(uint8_t code) {
  if (first_classified_ms == 0) {
    first_classified_ms = curr_tick();
  }
  switch (code) {
    case 0:
      classified_items.white++;
      break;
    case 1:
      classified_items.black++;
      break;
    case 2:
      classified_items.alumn++;
      break;
    case 3:
      classified_items.steel++;
      break;
    default:
      break;
  }
}

void log_item_deposited(uint8_t code) {
  last_deposited_ms = curr_tick();
  switch (code) {
    case 0:
      deposited_items.white++;
      break;
    case 1:
      deposited_items.black++;
      break;
    case 2:
      deposited_items.alumn++;
      break;
    case 3:
      deposited_items.steel++;
      break;
    default:
      break;
  }
}

uint16_t sort_time_ms() {
  return last_deposited_ms - first_classified_ms;
}

void display_stats() {
  char line[24];
  LCDClear();
  snprintf(line, 24, "%02dW %02dB %02dA %02dS",
           deposited_items.white,
           deposited_items.black,
           deposited_items.alumn,
           deposited_items.steel);
  LCDWriteStringXY(0, 0, line);
  snprintf(line, 24, "%02dW %02dB %02dA %02dS",
           classified_items.white - deposited_items.white,
           classified_items.black - deposited_items.black,
           classified_items.alumn - deposited_items.alumn,
           classified_items.steel - deposited_items.steel);
  LCDWriteStringXY(0, 1, line);
  uart_printf("sort time: %dms\n", sort_time_ms());
}
