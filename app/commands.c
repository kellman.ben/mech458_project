#include "commands.h"
#include "daq.h"
#include "stepper_ctrl.h"
#include "belt_ctrl.h"
#include "sys_tick.h"
#include "sm.h"
#include <device/uart.h>
#include <stdlib.h>

#define PARSE_INT32(variable, idx) do{ \
  char *end = NULL; \
  variable = strtol(argv[idx + 1], &end, 0); \
  if(*end != '\0'){                  \
  uart_printf("Argument %u invalid, could not parse %s as an int\n", idx, argv[idx + 1]); \
  return  CLI_E_INVALID_ARGS;                                  \
  }                                     \
}while(0)

cli_status_t echo_func(int argc, char **argv);
cli_status_t save_config(int argc, char **argv);
cli_status_t load_config(int argc, char **argv);
cli_status_t write_daq_config(int argc, char **argv);
cli_status_t read_daq_config(int argc, char **argv);

cli_status_t stepper_move_func(int argc, char **argv);
cli_status_t stepper_home_func(int argc, char **argv);
cli_status_t belt_stop_func(int argc, char **argv);
cli_status_t belt_speed_func(int argc, char **argv);
cli_status_t belt_start_cmd(int argc, char **argv);
cli_status_t current_state_cmd(int argc, char **argv);

cli_status_t sort_time_cmd(int argc, char **argv);
cli_status_t stepper_plot(int argc, char **argv);

cli_status_t stepper_set_len(int argc, char **argv);
cli_status_t stepper_set_profile(int argc, char **argv);
cli_status_t stepper_set_delay(int argc, char **argv);

cmd_t cmds[] = {
    {.cmd = "echo", .func = echo_func},
    {.cmd = "write_daq_config", .func = write_daq_config},
    {.cmd = "read_daq_config", .func = read_daq_config},
    {.cmd = "save_config", .func = save_config},
    {.cmd = "load_config", .func = load_config},
    {.cmd = "stepper_move", .func = stepper_move_func},
    {.cmd = "stepper_home", .func = stepper_home_func},
    {.cmd = "belt_stop", .func = belt_stop_func},
    {.cmd = "belt_start", .func = belt_start_cmd},
    {.cmd = "belt_speed", .func = belt_speed_func},
    {.cmd = "curr_state", .func = current_state_cmd},
    {.cmd = "sort_time_ms", .func = sort_time_cmd},
    {.cmd = "stepper_plot", .func = stepper_plot},
    {.cmd = "step_len", .func = stepper_set_len},
    {.cmd = "step_profile", .func = stepper_set_profile},
    {.cmd = "step_stop_dly", .func = stepper_set_delay},
};

cli_t cli_g = {
    .println = NULL,
    .cmd_tbl = cmds,
    .cmd_cnt = sizeof(cmds)/sizeof(cmd_t)
};

cli_status_t echo_func(int argc, char **argv) {
  uart_print(argv[0]);
  uart_print("\n");
  return CLI_OK;
}

cli_status_t save_config(int argc, char **argv) {
  daq_save_config();
  save_stepper_config();
  uart_println("Config Saved to EEPROM\n");
  return CLI_OK;
}

cli_status_t load_config(int argc, char **argv) {
  daq_load_config();
  load_stepper_config();
  uart_println("Config Loaded from EEPROM\n");
  return CLI_IDLE;
}

cli_status_t write_daq_config(int argc, char **argv) {
  if (argc != 4) {
    uart_printf("Expected 3 args\n");
    return CLI_E_INVALID_ARGS;
  }

  daq_config_t config;
  PARSE_INT32(config.white_thresh, 0);
  PARSE_INT32(config.steel_thresh, 1);
  PARSE_INT32(config.alumn_thresh, 2);

  daq_update_config(&config);
  return CLI_OK;
}

cli_status_t read_daq_config(int argc, char **argv) {
  const daq_config_t *config = daq_read_config();

  uart_printf("%d %d %d\n", config->white_thresh, config->steel_thresh, config->alumn_thresh);
  return CLI_IDLE;
}

cli_status_t stepper_move_func(int argc, char **argv) {
  uint8_t target_pos;

  if (argc < 2) {
    uart_printf("stepper_move requires at least one argument\n");
    return CLI_E_INVALID_ARGS;
  }

  PARSE_INT32(target_pos, 0);
  stepper_bounds_t bounds = {.cw_bound=target_pos, .ccw_bound=target_pos};
  if (argc == 4) {
    PARSE_INT32(bounds.cw_bound, 1);
    PARSE_INT32(bounds.ccw_bound, 2);
  }

  if (target_pos > 199 || target_pos < 0) {
    uart_printf("Argument 0 out of range [0 199]\n");
    return CLI_E_INVALID_ARGS;
  }
  uint16_t estimate = stepper_move(target_pos, &bounds);
  uart_printf("Estimated: %d ms\n", estimate);
  return CLI_IDLE;
}

cli_status_t stepper_home_func(int argc, char **argv) {
  stepper_home();
  return CLI_IDLE;
}

cli_status_t belt_stop_func(int argc, char **argv) {
  belt_stop();
  return CLI_IDLE;
}

cli_status_t belt_speed_func(int argc, char **argv) {
  if (argc != 2){
    uart_printf("Expected 2 arguments\n");
    return CLI_E_INVALID_ARGS;
  }
  int32_t speed;
  PARSE_INT32(speed, 0);
  if (speed > UINT8_MAX || speed < 0) {
    uart_printf("Argument out of bounds [0 255]\n");
    return CLI_E_INVALID_ARGS;
  }
  belt_speed_set(speed);
  return CLI_IDLE;
}

cli_status_t belt_start_cmd(int argc, char **argv) {
  belt_start();
  return CLI_IDLE;
}

cli_status_t current_state_cmd(int argc, char **argv) {
  uart_printf("current state: %s\n", sm_state_name[sm_state()]);
  return CLI_IDLE;
}

cli_status_t sort_time_cmd(int argc, char **argv) {
  uart_printf("sort time %dms\n", sort_time_ms());
  return CLI_IDLE;
}

cli_status_t stepper_plot(int argc, char** argv) {
  uint8_t endpoint;
  stepper_bounds_t waypoint_bounds, endpoint_bounds;
  uint16_t entry_ms, exit_ms;

  if (argc != 8){
    uart_printf("Incorrect args\n");
    return CLI_E_INVALID_ARGS;
  }

  PARSE_INT32(endpoint, 0);
  PARSE_INT32(waypoint_bounds.cw_bound, 1);
  PARSE_INT32(waypoint_bounds.ccw_bound, 2);
  PARSE_INT32(endpoint_bounds.cw_bound, 3);
  PARSE_INT32(endpoint_bounds.ccw_bound, 4);
  PARSE_INT32(entry_ms, 5);
  PARSE_INT32(exit_ms, 6);
  uart_printf("Parsed args\n");
  uint16_t time_to_target;
  bool ret = stepper_try_trajectory(endpoint, &waypoint_bounds, &endpoint_bounds, entry_ms, exit_ms, &time_to_target);
  if (ret){
    uart_printf("Reaching target zone in %dms\n", time_to_target);
  } else{
    uart_printf("Invalid Trajectory\n");
  }
  return CLI_IDLE;
}

cli_status_t stepper_set_len(int argc, char** argv) {
  if (argc != 3){
    uart_printf("Incorrect args\n");
    return CLI_E_INVALID_ARGS;
  }
  char mode = *argv[1];
  if (mode != 'a' && mode != 'd'){
    uart_printf("First arg must be 'a' or 'd'\n");
    return CLI_E_INVALID_ARGS;
  }
  uint8_t n;
  PARSE_INT32(n, 1);
  set_profile_len(mode, n);
  set_profile_len(mode, n);
  return CLI_IDLE;
}

cli_status_t stepper_set_profile(int argc, char** argv) {
  if (argc != 4){
    uart_printf("Incorrect args\n");
    return CLI_E_INVALID_ARGS;
  }

  char mode = *argv[1];
  if (mode != 'a' && mode != 'd'){
    uart_printf("First arg must be 'a' or 'd'\n");
    return CLI_E_INVALID_ARGS;
  }
  uint8_t i;
  uint16_t d;
  PARSE_INT32(i, 1);
  PARSE_INT32(d, 2);
  set_profile(mode, i, d);
  return CLI_IDLE;
}

cli_status_t stepper_set_delay(int argc, char** argv) {
  if (argc != 2){
    uart_printf("Incorrect args\n");
    return CLI_E_INVALID_ARGS;
  }

  uint16_t delay;
  PARSE_INT32(delay, 0);
  stepper_set_dir_change_delay(delay);
  return CLI_IDLE;
}


