#pragma once

typedef enum {
  SM_IDLE,
  SM_RUN,
  SM_RAMP,
  SM_STOPPED,
} sm_state_t;

extern const char **sm_state_name;

void sm_init();

void sm_process();

void log_item_classified(uint8_t code);

void log_item_deposited(uint8_t code);

uint16_t sort_time_ms();

sm_state_t sm_state();