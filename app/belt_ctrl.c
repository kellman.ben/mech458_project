#include "belt_ctrl.h"
#include "pin_defs.h"
#include "util/fast_cbuf.h"
#include "daq.h"
#include "util/debounce.h"
#include "device/uart.h"
#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/atomic.h>

/*
 * D4/G5/OCR0B -> PWM
 * D52/PB1 -> IA
 * D53/PB0 -> IB
 * D3/PE5/INT5 -> EX
 */

#define MOTOR_PINS (MOTOR_IA_MSK | MOTOR_IB_MSK)
#define MOTOR_STOP (MOTOR_IA_MSK | MOTOR_IB_MSK)
#define MOTOR_FWD (MOTOR_IB_MSK)
#define DEFAULT_BELT_SPEED (140u)

#define EXIT_PIN (PINE)
#define EXIT_IDX (PE5)

static volatile int8_t items_pending_deposit = 0;

static debounced_button_t exit_sensor = {
    .prev_state = BUTTON_HIGH,
    .debounce_state = 0xff,
    .dc_mask = 0xf0,
    .pin = &EXIT_PIN,
    .idx = EXIT_IDX,
};


bool belt_stopped() {
  return MOTOR_PORT==MOTOR_STOP;
}

void belt_init() {
  belt_stop();
  belt_speed_set(DEFAULT_BELT_SPEED);
  MOTOR_DDR |= MOTOR_PINS;

  /* Configure PWM */
  TCCR0A = 0x3;     /* Set timer 0 to fast PWM (WGM=3) */
  TCCR0A |= 2 << 4; /* Clear on compare match COM=2 */
  TCCR0B = 0x2;     /* Set clock prescaler to 8*/
  DDRG |= 1 << 5;   /* Set pin G5 to output */
}

void belt_stop() {
  MOTOR_PORT = MOTOR_STOP;
}

void belt_speed_set(uint8_t speed) {
  OCR0B = speed;
}

void belt_deposit_item() {
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    ++items_pending_deposit;
    // start the belt if the next item isn't blocking the exit sensor
    MOTOR_PORT = MOTOR_FWD;
  }
}

bool belt_items_past_exit() {
  bool ret;
  ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
    ret = (items_pending_deposit<=0);
  }
  return ret;
}

void belt_poll_exit() {
  button_state_t sensor_state = debounce_button(&exit_sensor);

  if (!fast_cbuf_empty(items_buffer_handle)){
    if (sensor_state == BUTTON_FALLING){
      // item entering the exit sensor
      if (items_pending_deposit <= 0) {
        uart_printf("Stopping belt\n");
        // We aren't ready to deposit this item yet
        MOTOR_PORT = MOTOR_STOP;
      }
    }
    if (sensor_state == BUTTON_RISING){
      // item leaving the exit sensor
      --items_pending_deposit;
    }
  }
}

void belt_start() {
  MOTOR_PORT = MOTOR_FWD;
}

bool motor_running() {
  if (MOTOR_PORT==MOTOR_FWD) {
    return true;
  } else {
    return false;
  }
}

void load_belt_speed() {

}

void save_belt_speed() {

}
