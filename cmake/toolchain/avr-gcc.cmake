#Find compiler, may need add the path of the avr gnu toolchain/avr dude on your pc for cmake to find it

find_program(AVR_CC avr-gcc
        PATHS "C:/Program Files (x86)/Atmel/Studio/7.0/toolchain/avr8/avr8-gnu-toolchain/bin"
        REQUIRED)

find_program(AVR_AS avr-as
        PATHS "C:/Program Files (x86)/Atmel/Studio/7.0/toolchain/avr8/avr8-gnu-toolchain/bin"
        REQUIRED)

find_program(AVR_CXX avr-g++
        PATHS "C:/Program Files (x86)/Atmel/Studio/7.0/toolchain/avr8/avr8-gnu-toolchain/bin"
        REQUIRED)

find_program(AVR_OBJCOPY avr-objcopy
        PATHS "C:/Program Files (x86)/Atmel/Studio/7.0/toolchain/avr8/avr8-gnu-toolchain/bin"
        REQUIRED)

find_program(AVR_SIZE_TOOL avr-size
        PATHS "C:/Program Files (x86)/Atmel/Studio/7.0/toolchain/avr8/avr8-gnu-toolchain/bin"
        REQUIRED)

find_program(AVR_OBJDUMP avr-objdump
        PATHS "C:/Program Files (x86)/Atmel/Studio/7.0/toolchain/avr8/avr8-gnu-toolchain/bin"
        REQUIRED)

find_program(AVR_DUDE avrdude
        PATHS "C:/AVRDUDE_Uploader"
        REQUIRED)

# Configure CMake to use the avr gcc toolchain
set(CMAKE_C_COMPILER ${AVR_CC})
set(CMAKE_CXX_COMPILER ${AVR_CXX})
set(CMAKE_ASM_COMPILER ${AVR_AS})
set(CMAKE_OBJCOPY ${AVR_OBJCOPY})
set(CMAKE_OBJDUMP ${AVR_OBJDUMP})
set(SIZE ${AVR_SIZE_TOOL})

# Limit library/inlcude search paths to avoid picking up system files (may not be needed)
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(CMAKE_SYSTEM_NAME Generic)

# Prevents compiler from failing to build default test file
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

add_compile_options(
        -mmcu=${AVR_MCU}
        -funsigned-char
        -funsigned-bitfields
        -ffunction-sections
        -fdata-sections
        -fpack-struct
        -fshort-enums
        -mrelax
)

add_link_options(
        -mmcu=${AVR_MCU}
        -Wl,-Map=app_mem.map
        -Wl,--start-group
        -Wl,-lm
        -Wl,--end-group
        -Wl,--gc-sections
        -mrelax
)