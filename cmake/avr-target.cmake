# Functions that allow for generating avr targets and disabling when cross compiling

if (NOT UPLOAD_BAUD)
    set(UPLOAD_BAUD 115200)
endif ()

function(add_avr_executable EXECUTABLE_NAME)
    if (NOT ARGN)
        message(FATAL_ERROR "No source files given for ${EXECUTABLE_NAME}.")
    endif (NOT ARGN)
    # set file names
    set(elf_file ${EXECUTABLE_NAME}.elf)
    set(hex_file ${EXECUTABLE_NAME}.hex)
    set(lst_file ${EXECUTABLE_NAME}.lst)
    set(map_file ${EXECUTABLE_NAME}.map)
    set(eeprom_image ${EXECUTABLE_NAME}${MCU_TYPE_FOR_FILENAME}-eeprom.hex)

    set(${EXECUTABLE_NAME}_ELF_TARGET ${elf_file} PARENT_SCOPE)
    set(${EXECUTABLE_NAME}_HEX_TARGET ${hex_file} PARENT_SCOPE)
    set(${EXECUTABLE_NAME}_LST_TARGET ${lst_file} PARENT_SCOPE)
    set(${EXECUTABLE_NAME}_MAP_TARGET ${map_file} PARENT_SCOPE)
    set(${EXECUTABLE_NAME}_EEPROM_TARGET ${eeprom_file} PARENT_SCOPE)
    # elf file
    add_executable(${elf_file} ${ARGN})

    add_custom_command(
            OUTPUT ${hex_file}
            COMMAND
            ${CMAKE_OBJCOPY} -j .text -j .data -O ihex ${elf_file} ${hex_file}
            COMMAND
            ${AVR_SIZE_TOOL} -C;--mcu=${AVR_MCU} ${elf_file}
            DEPENDS ${elf_file}
    )

    add_custom_command(
            OUTPUT ${lst_file}
            COMMAND
            ${CMAKE_OBJDUMP} -d ${elf_file} > ${lst_file}
            DEPENDS ${elf_file}
    )

    # eeprom
    add_custom_command(
            OUTPUT ${eeprom_image}
            COMMAND
            ${CMAKE_OBJCOPY} -j .eeprom --set-section-flags=.eeprom=alloc,load
            --change-section-lma .eeprom=0 --no-change-warnings
            -O ihex ${elf_file} ${eeprom_image}
            DEPENDS ${elf_file}
    )

    add_custom_target(
            ${EXECUTABLE_NAME}
            ALL
            DEPENDS ${hex_file} ${lst_file} ${eeprom_image}
    )

    set_target_properties(
            ${EXECUTABLE_NAME}
            PROPERTIES
            OUTPUT_NAME "${elf_file}"
    )

    # clean
    get_directory_property(clean_files ADDITIONAL_MAKE_CLEAN_FILES)

    # upload - with avrdude
    add_custom_target(
            upload_${EXECUTABLE_NAME}
            ${AVR_DUDE}
            -p atmega2560
            -c wiring
            -U flash:w:${hex_file}
            -P ${UPLOAD_PORT}
            -b ${UPLOAD_BAUD}
            -D
            DEPENDS ${hex_file}
            COMMENT "Uploading ${hex_file} to ${AVR_MCU} using ${AVR_PROGRAMMER}"
    )
endfunction()
