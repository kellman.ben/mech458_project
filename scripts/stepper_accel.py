import math
import time

import serial

t0 = 20e-3
v0 = 1 / t0
a = 750
t0 = math.sqrt(2 / a)
results = [int(t0 * 1e6)]

ser = serial.Serial('COM4', 9600, timeout=0.2)
time.sleep(2)
running = True
try:
    while running:
        vmax = float(input("Enter Vmax: "))
        a = float(input("Enter a: "))
        t0 = 20e-3
        v0 = 1 / t0
        results = [int(t0 * 1e6)]
        n = 0
        while True:
            c = (math.sqrt(v0 ** 2 + 2 * a * (n + 1)) - math.sqrt(v0 ** 2 + 2 * a * n)) / a
            if 1 / c > vmax:
                break
            results.append(int(c * 1e6))
            n += 1

        ser.write(f"step_len a {len(results)}\n".encode())
        time.sleep(0.01)
        ser.write(f"step_len d {len(results)}\n".encode())
        time.sleep(0.01)

        for idx, (a_delay, d_delay) in enumerate(zip(results, reversed(results))):
            ser.write(f"step_profile a {idx} {a_delay}\n".encode())
            time.sleep(0.01)
            ser.write(f"step_profile d {idx} {d_delay}\n".encode())
            time.sleep(0.01)


        print(results)
        print(list(reversed(results)))
        stop = 'r'
        while stop == 'r':
            ser.write('stepper_move 100\n'.encode())
            time.sleep(2)
            ser.write('stepper_move 0\n'.encode())
            time.sleep(2)
            stop = input("'s' to stop, r to replay, enter to continue: ")
            running = stop != 's'
finally:
    ser.write("save_config\n".encode())
    print("Done")
