import time
from pprint import pprint

import serial

ser = serial.Serial('COM4', 9600, timeout=0.2)
time.sleep(2)

for _ in range(1):
    ser.write('stepper_move 100\n'.encode())
    time.sleep(0.25)
    ser.write('stepper_move 0\n'.encode())
    time.sleep(1)

pprint(ser.readlines())
ser.close()
