import csv
import threading
import time

import matplotlib.pyplot as plt
import numpy as np
import serial
from mlxtend.plotting import plot_decision_regions
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler

thread_running = True

current_class = 0

features = []
classes = []
ser = serial.Serial('COM4', 9600)


def serial_thread():
    global thread_running
    global current_class

    while thread_running:
        try:
            data = ser.readline().decode('ascii').strip(">> ")
            min_value = int(data[4:8])
            mean_value = int(data[15:19])
            features.append([min_value])
            classes.append(current_class)
        except ValueError:
            pass


time.sleep(2)
ser.write("belt_move 150\n".encode())

threading.Thread(target=serial_thread, daemon=True).start()

input("Accepting White items, Press any key to input Group 1 items\n")
current_class = 1
input("Accepting Black items, Press any key to input Group 2 items\n")
current_class = 2
input("Accepting Aluminum items, Press any key to input Group 3 items\n")
current_class = 3
input("Accepting Steel items, Press any key to finish\n")
current_class = 4

thread_running = False

data = [[y, *x] for y, x in zip(classes, features)]

with open("test_day_data.csv", 'w', newline='') as f:
    w = csv.writer(f)
    w.writerows(data)

features = np.array(features)
y = np.array(classes)
scaler = StandardScaler().fit(features)
X = scaler.transform(features)
model = LogisticRegression(penalty='none', multi_class='multinomial').fit(X, y)

w_bar = model.coef_
b_bar = model.intercept_

w = w_bar / scaler.scale_
b = b_bar - np.sum(w_bar * (scaler.mean_ / scaler.scale_), 1)

w_max = np.max(np.abs(w))
w = np.int8(w / w_max * 127)
b = np.int32(b / w_max * 127)

model.coef_ = w
model.intercept_ = b

msg = "write_daq_config"

for wi in w.flat:
    msg += f" {wi}"

for bi in b.flat:
    msg += f" {bi}"
msg += "\n"

# ser.write(msg.encode('ascii'))
# ser.write("save_config\n".encode('ascii'))
time.sleep(0.5)
ser.write("belt_stop\n".encode('ascii'))
print(msg)
ser.close()

plot_decision_regions(np.array(features), np.array(classes), clf=model)

plt.show()
