#include <device/uart.h>
#include "util/cli.h"
#include <util/fast_cbuf.h>
#include <stddef.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <assert.h>

#define BUFFER_SIZE 254

#define FOSC 8000000// Clock Speed
#define BAUD 9600
#define MYUBRR (FOSC/16/BAUD-1)

static_assert(BUFFER_SIZE <= 254, "Buffer size must be smaller tha 254 bytes");

typedef struct{
  volatile uint8_t write_idx;
  volatile uint8_t read_idx;
  uint8_t size_mask;
  uint8_t storage[256];
}uint8_buff_t;


static cli_t* cli_handle;
static uint8_buff_t* tx_buffer_handle = NULL;
static bool uart_init_ = false;
static uint8_buff_t tx_buffer;

static void uart_putc(char c);
static bool uart_transmitting();

void uart_init(cli_t *cli_instance) {
  if (!uart_init_){
    tx_buffer_handle = &tx_buffer;
    tx_buffer.write_idx = 0;
    tx_buffer.size_mask = 0xff;
    tx_buffer.read_idx = 0;

    UBRR0H = (uint8_t) (MYUBRR >> 8);
    UBRR0L = (uint8_t) (MYUBRR);

    UCSR0B = (1 << RXCIE0) /* Enable RX complete interrupt */
             | (1 << RXEN0) /* Enable RX */
             | (1 << TXEN0);  /* Enable TX */

    UCSR0C = (3 << UCSZ00);
    uart_init_ = true;

    cli_handle = cli_instance;
    cli_handle->println = uart_print;
    cli_status_t cli_init_status = cli_init(cli_handle);
    assert(cli_init_status == CLI_OK);
  }
}

void uart_printf(const char *fmt, ...) {
  char msg[BUFFER_SIZE+1];
  va_list ap;
  va_start(ap, fmt);

  vsnprintf(msg, BUFFER_SIZE, fmt, ap);
  uart_print(msg);
}

void uart_println(const char *msg) {
  uart_print(msg);
  uart_putc('\n');
}

void uart_print(const char *msg) {
  const char* c = msg;
  while ((*c != '\0') && (c < &msg[BUFFER_SIZE])){
    uart_putc(*c);
    ++c;
  }
}

void uart_putc(char c) {
  if (!uart_transmitting()) {
    UDR0 = c;
    UCSR0B |= 1 << UDRIE0;
  } else {
    if (!fast_cbuf_full(tx_buffer_handle)){
      fast_cbuf_push(tx_buffer_handle, c);
    }
  }
}

bool uart_transmitting() {
  return !(UCSR0A & (1 << UDRE0));
}

extern void __assert(const char *__func, const char *__file,
              int __lineno, const char *__sexp){
  const char* file_name = __file;
  const char* c = __file;

  /* Get the file name */
  while (*c != '\0'){
    if (*c == '/' && *(c+1) != '\0') {
      file_name = c+1;
    }
    c++;
  }
  uart_printf("%s:%d: Assertion '%s' failed in function %s\n", file_name, __lineno, __sexp, __func);
}

ISR(USART0_RX_vect) {
  uint8_t data = UDR0;
  cli_put(cli_handle, data);
}

ISR(USART0_UDRE_vect) {
  if (fast_cbuf_empty(tx_buffer_handle)) {
    UCSR0B &= ~(1 << UDRIE0);
  } else {
    uint8_t data = fast_cbuf_front(tx_buffer_handle);
    fast_cbuf_pop(tx_buffer_handle);
    UDR0 = data;
  }
}