#include <util/lr_classifier.h>

uint8_t lr_classify(uint8_t num_features, uint8_t num_classes, const int8_t *w, const int32_t *b, const uint16_t *x) {
  /* init to min values so first iteration will always be larger */
  int32_t max_value = INT32_MIN;
  uint8_t arg_max = 0;

  /* Do matrix multiplication of w*x find the classification with the largest score */
  for (uint8_t i = 0; i < num_classes; ++i) {
    /* Score for ith classification */
    int32_t score = 0;
    /* Computing wi*x */
    for (uint8_t j = 0; j < num_features; ++j) {
      score += ((int32_t) x[j])*((int32_t) w[i*num_features + j]);
    }
    score += b[i];
    /* Update the best score */
    if (score >= max_value){
      max_value = score;
      arg_max = i;
    }
  }

  return arg_max;
}
