#include "util/debounce.h"

button_state_t debounce_button(debounced_button_t *button) {
  uint8_t pin_reading = *button->pin;
  button->debounce_state <<= 1;
  button->debounce_state |= (pin_reading & (1 << button->idx)) >> button->idx;
  button->debounce_state |= button->dc_mask;
  button_state_t curr_state = button->prev_state;

  if (button->debounce_state == button->dc_mask) {
    // Button has been low for the required number of samples
    curr_state = BUTTON_LOW;
    if (button->prev_state == BUTTON_HIGH) {
      curr_state = BUTTON_FALLING;
    }
    button->prev_state = BUTTON_LOW;
  } else if (button->debounce_state == 0xff) {
    // Button has been high for the reqquired number of smaples
    curr_state = BUTTON_HIGH;
    if (button->prev_state == BUTTON_LOW) {
      curr_state = BUTTON_RISING;
    }
    button->prev_state = BUTTON_HIGH;
  }

  return curr_state;
}
