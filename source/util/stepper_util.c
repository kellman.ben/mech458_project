#include <util/stepper_util.h>
#include <util/fast_cbuf.h>
#include <stddef.h>
#include <stdlib.h>

// Utility Function Defs
static trapezoidal_path_t compute_trapezoidal_trajectory(const stepper_profile_t* profile, uint8_t num_steps);
static trapezoidal_path_t compute_stopping_trajectory(const stepper_profile_t* profile, uint16_t initial_period);
static uint8_t mod200(int16_t angle);
static bool point_in_bounds(const stepper_bounds_t* bounds, uint8_t point);
static uint16_t time_to_point(const trapezoidal_path_t* path, uint8_t point);
static uint16_t trapezoidal_path_duration(const trapezoidal_path_t* path);
static void initial_vel_offset_profile(const stepper_profile_t* profile,
                                       const stepper_state_t* initial_state,
                                       stepper_profile_t* offset_profile);
static int16_t signed_distance(uint8_t a, uint8_t b);
static uint8_t stopping_distance(const stepper_profile_t* profile, uint16_t initial_period);
static uint8_t dist_to_point(uint8_t p1, uint8_t p2, stepper_dir_t dir);

static uint8_t plot_continuous_path(const stepper_profile_t* profile,
                                    const stepper_state_t* initial_state,
                                    uint8_t abs_dist,
                                    trapezoidal_path_t* path_out);

static uint8_t plot_dir_change_path(const stepper_profile_t* profile,
                                    const stepper_state_t* initial_state,
                                    uint8_t abs_dist,
                                    trapezoidal_path_t* path_out);

uint8_t stepper_plan_endpoint_path(const stepper_profile_t* profile,
                                   const stepper_state_t* initial_state,
                                   uint8_t target,
                                   trapezoidal_path_t path_out[2]) {
  uint8_t initial_pos = initial_state->abs_position;

  // trivial case, no path plotting needed
  if ((target==initial_state->abs_position) && (initial_state->direction==STEPPER_DIR_STOP)) {
    return 0;
  }

  ///////////////////////////////////////////////////////////////
  //// Step 1. Compute distances and initial velocity offset ////
  ///////////////////////////////////////////////////////////////


  int16_t signed_dist = signed_distance(target, initial_pos);
  stepper_dir_t dir = signed_dist > 0 ? STEPPER_DIR_CW : STEPPER_DIR_CCW;
  uint8_t abs_dist = abs(signed_dist);

  // If starting from stopped, a start step will be executed immediately
  if (initial_state->direction==STEPPER_DIR_STOP) {
    abs_dist--;
  }

  // Find an appropriate offset for the acceleration table so that we can treat the system as having 0
  // initial velocity (ONLY APPLIES if we don't need to change directions)
  stepper_profile_t offset_profile;
  initial_vel_offset_profile(profile, initial_state, &offset_profile);

  ///////////////////////////////////////
  //// Step 2. Compute shortest path ////
  ///////////////////////////////////////

  uint8_t path_idx = 0;

  if (initial_state->direction!=STEPPER_DIR_STOP) {

    if ((initial_state->direction==dir) || (abs_dist==0)) {
      path_idx += plot_continuous_path(profile, initial_state, abs_dist, path_out);
    } else {
      trapezoidal_path_t continuous_path[2];
      uint8_t continuous_len = plot_continuous_path(profile, initial_state, 200 - abs_dist, continuous_path);

      trapezoidal_path_t dir_change_path[2];
      uint8_t dir_change_len = plot_dir_change_path(profile, initial_state, abs_dist, dir_change_path);

      uint16_t dir_change_ms = 0;
      for (uint8_t i = 0; i < dir_change_len; ++i) {
        dir_change_ms += dir_change_path[i].duration_ms;
      }

      uint16_t continuous_ms = 0;
      for (uint8_t i = 0; i < continuous_len; ++i) {
        continuous_ms += continuous_path[i].duration_ms;
      }

      trapezoidal_path_t* path;
      if (continuous_ms < dir_change_ms) {
        path_idx = continuous_len;
        path = continuous_path;
      } else {
        path_idx = dir_change_len;
        path = dir_change_path;
      }

      for (uint8_t i = 0; i < path_idx; ++i) {
        path_out[i] = path[i];
      }
    }
  } else {
    // compute the shortest path
    trapezoidal_path_t path = compute_trapezoidal_trajectory(&offset_profile, abs_dist);
    path.direction = dir;
    path_out[path_idx++] = path;

  }

  return path_idx;
}

stepper_dir_t stepper_pop_next_step(stepper_cmd_queue_t* cmd_queue, volatile uint16_t* step_period) {
  if (fast_cbuf_empty(cmd_queue)) {
    return STEPPER_DIR_STOP;
  }

  stepper_cmd_t* current_cmd = &fast_cbuf_front(cmd_queue);
  stepper_dir_t ret = current_cmd->dir;

  while (current_cmd->steps_remaining==0) {
    fast_cbuf_pop(cmd_queue);
    if (fast_cbuf_empty(cmd_queue)) {
      return ret;
    }
    current_cmd = &fast_cbuf_front(cmd_queue);
  }

  if (current_cmd->mode==STEPPER_MODE_WAIT) {
    if (current_cmd->steps_remaining > 65) {
      *step_period = 65000;
      current_cmd->steps_remaining -= 65;
    } else {
      *step_period = 1000*current_cmd->steps_remaining;
      fast_cbuf_pop(cmd_queue);
    }
  }

  if (current_cmd->mode==STEPPER_MODE_ACCELERATE) {
    /* Update isr period */
    uint16_t next_delay_us = *current_cmd->step_profile;
    *step_period = next_delay_us;
    current_cmd->step_profile++;
  }

  current_cmd->steps_remaining--;
  return ret;
}

trapezoidal_path_t compute_trapezoidal_trajectory(const stepper_profile_t* profile, uint8_t num_steps) {
  trapezoidal_path_t path = {.accel_steps=0, .decel_steps=0, .coast_steps=0, .duration_ms=0, .initial_delay_ms=0};
  // Regular step planning algorithm assumes that there are at least 3 steps
  if (num_steps==0) {
    return path;
  }

  if (num_steps < 2) {
    path.accel_steps = num_steps;
    path.duration_ms = (num_steps*profile->accel_table[0] + 500)/1000;
    path.accel_table = profile->accel_table;
    return path;
  }

  uint8_t total_steps;
  const uint16_t* step_period = &profile->accel_table[profile->accel_table_length - 1];
  if (profile->accel_table_length > 1) {
    path.accel_steps = profile->accel_table_length;
    path.decel_steps = stopping_distance(profile, *step_period);
    total_steps = path.accel_steps + path.decel_steps;

    // Decrease the time accelerating until accel + decel time is <= the number of steps to take
    while ((path.accel_steps > 0) && (total_steps > num_steps)) {
      --path.accel_steps;
      --step_period;
      path.decel_steps = stopping_distance(profile, *step_period);
      total_steps = path.accel_steps + path.decel_steps;
    }
  } else {
    step_period = profile->accel_table;
    path.decel_steps = stopping_distance(profile, *step_period);
    total_steps = path.decel_steps;
  }

  path.coast_period = *step_period;
  path.coast_steps = num_steps - total_steps;

  path.decel_table = &profile->decel_table[profile->decel_table_length - path.decel_steps];
  path.accel_table = profile->accel_table;

  path.duration_ms = trapezoidal_path_duration(&path);
  return path;
}

trapezoidal_path_t compute_stopping_trajectory(const stepper_profile_t* profile, uint16_t initial_period) {
  trapezoidal_path_t path = {
      .accel_steps=0,
      .coast_steps=0,
      .decel_steps=stopping_distance(profile, initial_period),
      .duration_ms=0,
      .initial_delay_ms=0,
  };
  path.decel_table = &profile->decel_table[profile->decel_table_length - path.decel_steps];
  uint32_t duration_us = 0;
  for (uint8_t i = 0; i < path.decel_steps; ++i) {
    duration_us += profile->decel_table[profile->decel_table_length - i - 1];
  }
  path.duration_ms = (uint16_t) ((duration_us + 500u)/1000u);
  return path;
}

void stepper_queue_trajectory(const trapezoidal_path_t* path,
                              stepper_cmd_queue_t* cmd_queue) {
  stepper_cmd_t stepper_cmd;

  if (path->initial_delay_ms > 0) {
    uint16_t delay = path->initial_delay_ms;
    stepper_cmd.mode = STEPPER_MODE_WAIT;
    stepper_cmd.dir = STEPPER_DIR_NOP;
    while (delay > 0) {
      if (delay > 255) {
        stepper_cmd.steps_remaining = 255;
      } else {
        stepper_cmd.steps_remaining = delay;
      }
      delay -= stepper_cmd.steps_remaining;
      fast_cbuf_push(cmd_queue, stepper_cmd);
    }
  }

  if (path->accel_steps > 0) {
    stepper_cmd.step_profile = path->accel_table;
    stepper_cmd.mode = STEPPER_MODE_ACCELERATE;
    stepper_cmd.dir = path->direction;
    stepper_cmd.steps_remaining = path->accel_steps;
    fast_cbuf_push(cmd_queue, stepper_cmd);
  }

  if (path->coast_steps > 0) {
    stepper_cmd.steps_remaining = path->coast_steps;
    stepper_cmd.mode = STEPPER_MODE_COAST;
    stepper_cmd.dir = path->direction;
    stepper_cmd.step_profile = NULL;
    fast_cbuf_push(cmd_queue, stepper_cmd);
  }

  if (path->decel_steps > 0) {
    stepper_cmd.step_profile = path->decel_table;
    stepper_cmd.mode = STEPPER_MODE_ACCELERATE;
    stepper_cmd.dir = path->direction;
    stepper_cmd.steps_remaining = path->decel_steps;
    fast_cbuf_push(cmd_queue, stepper_cmd);
  }
}

void stepper_find_future_state(const stepper_cmd_queue_t* cmd_queue, stepper_state_t* state, uint16_t time_ms) {
  if (fast_cbuf_empty(cmd_queue) || time_ms==0) {
    return;
  }

  int32_t time_remaining_us = (int32_t) time_ms*1000;
  const uint16_t* period = &state->step_period;

  uint8_t n = fast_cbuf_size(cmd_queue);
  for (uint8_t i = 0; i < n; ++i) {
    const stepper_cmd_t* cmd = &fast_cbuf_at(cmd_queue, i);
    state->direction = cmd->dir;

    if (cmd->mode==STEPPER_MODE_ACCELERATE) {
      period = cmd->step_profile;
    }

    for (uint8_t j = 0; j < cmd->steps_remaining; ++j) {
      switch (cmd->mode) {
        case STEPPER_MODE_ACCELERATE:
          time_remaining_us -= *period;
          state->step_period = *period;
          state->abs_position = mod200((int16_t) state->abs_position + cmd->dir);
          ++period;
          break;
        case STEPPER_MODE_COAST:
          time_remaining_us -= *period;
          state->abs_position = mod200((int16_t) state->abs_position + cmd->dir);
          break;
        case STEPPER_MODE_WAIT:
          time_remaining_us -= 1000*(int32_t) cmd->steps_remaining;
          break;
      }
      if (time_remaining_us <= 0) {
        return;
      }
    }
  }
}

static uint8_t stopping_distance(const stepper_profile_t* profile, uint16_t initial_period) {
  uint8_t stopping_dist = 0;

  while (profile->decel_table[profile->decel_table_length - stopping_dist - 1] > initial_period) {
    if (stopping_dist >= profile->decel_table_length) {
      return profile->decel_table_length;
    }
    stopping_dist++;
  }

  return stopping_dist;
}

void stepper_queue_start(stepper_dir_t direction, stepper_cmd_queue_t* cmd_queue) {
  stepper_cmd_t stepper_cmd;
  stepper_cmd.mode = STEPPER_MODE_COAST;
  stepper_cmd.dir = direction;
  stepper_cmd.steps_remaining = 0;
  fast_cbuf_push(cmd_queue, stepper_cmd);
}

static bool point_in_bounds(const stepper_bounds_t* bounds, uint8_t point) {
  return mod200((int16_t) point - bounds->cw_bound) <= mod200((int16_t) bounds->ccw_bound - bounds->cw_bound);
}

uint8_t mod200(int16_t angle) {
  angle %= 200;
  if (angle < 0) {
    angle += 200;
  }
  assert(angle >= 0);
  assert(angle < 200);
  return angle;
}

uint16_t time_to_point(const trapezoidal_path_t* path, uint8_t point) {
  uint32_t duration_us = ((uint32_t)path->initial_delay_ms) * 1000;
  const uint16_t* step_period = path->accel_table;

  if (path->accel_steps > 0) {
    for (uint8_t i = 0; i < path->accel_steps && point > 0; ++i) {
      duration_us += *step_period;
      --point;
      ++step_period;
    }
    --step_period;
  }

  if (point < path->coast_steps) {
    duration_us += (uint32_t) (*step_period) * point;
    point = 0;
  } else if (path->coast_steps > 0) {
    duration_us += (uint32_t) (*step_period) * path->coast_steps;
    point -= path->coast_steps;
  }

  step_period = path->decel_table;
  for (uint8_t i = 0; i < path->decel_steps && point > 0; ++i) {
    duration_us += *step_period;
    ++step_period;
    --point;
  }

  if (point!=0) {
    return INFINITE_WAIT_TIME_MS;
  }

  return (duration_us + 500) / 1000;
}

int16_t signed_distance(uint8_t a, uint8_t b) {
  int16_t dist = (int16_t) a - b;
  if (dist > 100) {
    dist = dist - 200;
  } else if (dist < -100) {
    dist = 200 + dist;
  }
  return dist;
}

void initial_vel_offset_profile(const stepper_profile_t* profile,
                                const stepper_state_t* initial_state,
                                stepper_profile_t* offset_profile) {
  const uint16_t* accel_table_start = profile->accel_table;
  uint8_t accel_offset = 0;
  if (initial_state->direction!=STEPPER_DIR_STOP) {
    while ((accel_offset < profile->accel_table_length) && (*accel_table_start >= initial_state->step_period)) {
      ++accel_table_start;
      ++accel_offset;
    }
    // make sure that the table start is always within the table, even for length 0 tables
    if (accel_offset >= profile->accel_table_length) {
      accel_table_start = &profile->accel_table[profile->accel_table_length - 1];
    }
  } else {
    accel_offset = profile->starting_speed_idx;
    accel_table_start = &profile->accel_table[profile->starting_speed_idx];
  }

  offset_profile->accel_table = accel_table_start;
  offset_profile->decel_table = profile->decel_table;
  offset_profile->accel_table_length = profile->accel_table_length - accel_offset;
  offset_profile->decel_table_length = profile->decel_table_length;
}

uint16_t trapezoidal_path_duration(const trapezoidal_path_t* path) {

  uint32_t duration = 0;
  for (uint8_t i = 0; i < path->accel_steps; ++i) {
    duration += path->accel_table[i];
  }

  duration += (uint32_t) path->coast_period*path->coast_steps;

  for (uint8_t i = 0; i < path->decel_steps; ++i) {
    duration += path->decel_table[i];
  }

  return (duration + 500)/1000;
}

uint8_t dist_to_point(uint8_t p1, uint8_t p2, stepper_dir_t dir) {
  return mod200(dir*((int16_t) p2 - p1));
}

uint8_t plot_continuous_path(const stepper_profile_t* profile,
                             const stepper_state_t* initial_state,
                             uint8_t abs_dist,
                             trapezoidal_path_t* path_out) {

  assert(initial_state->direction!=STEPPER_DIR_STOP);

  uint8_t path_idx = 0;
  stepper_profile_t offset_profile;
  initial_vel_offset_profile(profile, initial_state, &offset_profile);
  stepper_dir_t dir = initial_state->direction;

  trapezoidal_path_t breaking_path = compute_stopping_trajectory(profile, initial_state->step_period);
  breaking_path.direction = initial_state->direction;

  // Going to overshoot
  if (breaking_path.decel_steps > abs_dist) {
    abs_dist = breaking_path.decel_steps - abs_dist;
    offset_profile.accel_table = profile->accel_table;
    offset_profile.accel_table_length = profile->accel_table_length;
    dir *= -1;
    path_out[path_idx++] = breaking_path;
  }

  trapezoidal_path_t path = compute_trapezoidal_trajectory(&offset_profile, abs_dist);
  path.direction = dir;
  path_out[path_idx++] = path;

  return path_idx;
}

uint8_t plot_dir_change_path(const stepper_profile_t* profile,
                             const stepper_state_t* initial_state,
                             uint8_t abs_dist,
                             trapezoidal_path_t* path_out) {
  uint8_t path_idx = 0;
  stepper_dir_t dir = initial_state->direction;

  assert(initial_state->direction!=STEPPER_DIR_STOP);

  path_out[path_idx] = compute_stopping_trajectory(profile, initial_state->step_period);
  path_out[path_idx].direction = dir;
  dir *= -1;
  abs_dist += path_out[path_idx].decel_steps;
  path_idx++;
  trapezoidal_path_t path = compute_trapezoidal_trajectory(profile, abs_dist);
  path.direction = dir;
  path.initial_delay_ms = profile->dir_change_delay_ms;
  path_out[path_idx++] = path;

  return path_idx;
}

void time_to_bounds(uint8_t n,
                    trapezoidal_path_t* path,
                    uint8_t initial_pos,
                    const stepper_bounds_t* bounds,
                    bound_crossings_t* bounds_crossings) {
  bool in_bounds = point_in_bounds(bounds, initial_pos);

  bounds_crossings->exit_time = INFINITE_WAIT_TIME_MS;
  bounds_crossings->entry_time = INFINITE_WAIT_TIME_MS;
  uint16_t prev_path_times = 0;

  if (in_bounds) {
    bounds_crossings->entry_time = 0;
  }

  for (uint8_t i = 0; i < n; ++i) {
    stepper_dir_t d = path[i].direction;
    uint8_t cw_dist = dist_to_point(initial_pos, bounds->cw_bound, d);
    uint8_t ccw_dist = dist_to_point(initial_pos, bounds->ccw_bound, d);
    uint16_t cw_bounds_enter = time_to_point(&path[i], cw_dist);
    uint16_t ccw_bounds_enter = time_to_point(&path[i], ccw_dist);

    if (cw_bounds_enter!=INFINITE_WAIT_TIME_MS || ccw_bounds_enter!=INFINITE_WAIT_TIME_MS) {
      // first time crossing a bound will be exit if starting in the bounds
      uint16_t tmin, tmax;
      uint8_t tmin_step, tmax_step;
      if (cw_bounds_enter < ccw_bounds_enter) {
        tmin = cw_bounds_enter;
        tmax = ccw_bounds_enter;
        tmin_step = cw_dist;
        tmax_step = ccw_dist;
      } else {
        tmin = ccw_bounds_enter;
        tmax = cw_bounds_enter;
        tmin_step = ccw_dist;
        tmax_step = cw_dist;
      }

      if (in_bounds) {
        if (tmax != INFINITE_WAIT_TIME_MS) {
          bounds_crossings->entry_time = tmax + prev_path_times;
          bounds_crossings->entry_step = tmax_step;
          bounds_crossings->entry_idx = i;
        }
        if (tmin != INFINITE_WAIT_TIME_MS) {
          bounds_crossings->exit_time = tmin + prev_path_times;
          bounds_crossings->exit_step = tmax_step;
          bounds_crossings->exit_idx = i;
        }
      } else {
        if (tmin != INFINITE_WAIT_TIME_MS) {
          bounds_crossings->entry_time = tmin + prev_path_times;
          bounds_crossings->entry_step = tmin_step;
          bounds_crossings->entry_idx = i;
        }
        if (tmax != INFINITE_WAIT_TIME_MS) {
          bounds_crossings->exit_time = tmax + prev_path_times;
          bounds_crossings->exit_step = tmax_step;
          bounds_crossings->exit_idx = i;
        }
      }
      // passed through both bounds
      if (cw_bounds_enter!=INFINITE_WAIT_TIME_MS && ccw_bounds_enter!=INFINITE_WAIT_TIME_MS) {
        in_bounds = false;
      } else {
        in_bounds = !in_bounds;
      }
    }
    prev_path_times += path[i].duration_ms + path[i].initial_delay_ms;
    initial_pos = mod200((int16_t) initial_pos +
        path[i].direction*(path[i].accel_steps + path[i].coast_steps + path[i].decel_steps));
  }
}

