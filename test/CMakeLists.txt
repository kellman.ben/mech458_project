add_compile_options(-fsanitize=address)
add_link_options(-fsanitize=address)
add_compile_options(-fsanitize=bounds-strict)
add_link_options(-fsanitize=bounds-strict)

add_compile_options(-fsanitize=undefined)
add_link_options(-fsanitize=undefined)
set(CMAKE_CXX_STANDARD 17)

add_executable(test_demo
        test_demo.cpp)

add_executable(test_circular_buffer
        test_circular_buffer.cpp)
target_link_libraries(test_circular_buffer PUBLIC util)

add_executable(test_one_vs_all
        test_one_vs_all.cpp)
target_link_libraries(test_one_vs_all PUBLIC util)

add_executable(test_stepper
        test_stepper_util.cpp)
target_link_libraries(test_stepper PUBLIC util)

add_executable(test_debounce
        test_debounce.cpp)
target_link_libraries(test_debounce PUBLIC util)