#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include <util/fast_cbuf.h>

typedef struct {
  uint8_t write_idx;
  uint8_t read_idx;
  uint8_t size_mask;
  uint8_t storage[];
} cbuf_uint8_t;

typedef cbuf_uint8_t* cbuf_uint8_handle_t;

TEST_CASE("Circular buffer init"){
  char buff[sizeof(cbuf_uint8_t)+4];
  auto handle = reinterpret_cast<cbuf_uint8_handle_t>(&buff[0]);
  fast_cbuf_init(handle, 3);

  CHECK(fast_cbuf_empty(handle));
  CHECK(!fast_cbuf_full(handle));
}

TEST_CASE("Circular buffer push pop") {
  char buff[sizeof(cbuf_uint8_t)+4];
  auto handle = reinterpret_cast<cbuf_uint8_handle_t>(&buff[0]);
  fast_cbuf_init(handle, 3);
  handle->size_mask = 0x3;


  fast_cbuf_push(handle, 1);
  fast_cbuf_push(handle, 2);
  fast_cbuf_push(handle, 3);

  CHECK(fast_cbuf_size(handle) == 3);
  CHECK(fast_cbuf_front(handle)==1);
  fast_cbuf_pop(handle);
  CHECK(fast_cbuf_front(handle)==2);
  fast_cbuf_pop(handle);
  CHECK(fast_cbuf_front(handle)==3);
  fast_cbuf_pop(handle);

  fast_cbuf_push(handle, 4);
  fast_cbuf_push(handle, 5);
  fast_cbuf_push(handle, 6);

  CHECK(fast_cbuf_size(handle) == 3);

  CHECK(fast_cbuf_at(handle, 0) == 4);
  CHECK(fast_cbuf_at(handle, 1) == 5);
  CHECK(fast_cbuf_at(handle, 2) == 6);

  CHECK(fast_cbuf_front(handle)==4);
  fast_cbuf_pop(handle);
  CHECK(fast_cbuf_front(handle)==5);
  fast_cbuf_pop(handle);
  CHECK(fast_cbuf_front(handle)==6);
  fast_cbuf_pop(handle);

  fast_cbuf_push(handle, 7);
  fast_cbuf_push(handle, 8);

  CHECK(fast_cbuf_size(handle) == 2);
  CHECK(fast_cbuf_front(handle)==7);
  fast_cbuf_pop(handle);
  CHECK(fast_cbuf_front(handle)==8);
  fast_cbuf_pop(handle);

}

TEST_CASE("Full Circular Buffer") {
  char buff[sizeof(cbuf_uint8_t)+4];
  auto handle = reinterpret_cast<cbuf_uint8_handle_t>(&buff[0]);
  fast_cbuf_init(handle, 3);
  handle->size_mask = 0x3;

  CHECK(fast_cbuf_size(handle) == 0);


  fast_cbuf_push(handle, 1);
  CHECK(fast_cbuf_size(handle) == 1);
  fast_cbuf_push(handle, 2);
  CHECK(fast_cbuf_size(handle) == 2);
  fast_cbuf_push(handle, 3);
  CHECK(fast_cbuf_size(handle) == 3);

  CHECK(!fast_cbuf_empty(handle));
  CHECK(fast_cbuf_full(handle));
  CHECK(fast_cbuf_size(handle) == 3);

  CHECK(fast_cbuf_at(handle, 0) == 1);
  CHECK(fast_cbuf_at(handle, 1) == 2);
  CHECK(fast_cbuf_at(handle, 2) == 3);

}