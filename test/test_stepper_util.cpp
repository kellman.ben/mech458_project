#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include <util/stepper_util.h>
#include <util/fast_cbuf.h>

std::vector<uint16_t> accel_profile
    {20000, 12360, 9545, 8013, 7024, 6322, 5792, 5374, 5033, 4749, 4508, 4299, 4117, 3956, 3812, 3683, 3566, 3459};

// todo: test asymmetric acceleration profiles

TEST_CASE("cmd execution")
{
  uint8_t buffer[sizeof(stepper_cmd_queue_t) + 8*sizeof(stepper_cmd_t)];
  auto* cmd_queue_handle = static_cast<stepper_cmd_queue_t*>((void*) &buffer);
  cmd_queue_handle->read_idx = 0;
  cmd_queue_handle->write_idx = 0;
  cmd_queue_handle->size_mask = 0x7;
  uint16_t step_period = 0;

  std::vector<uint16_t> decel_profile{accel_profile};
  std::reverse(decel_profile.begin(), decel_profile.end());

  stepper_profile_t stepper_info{
      .accel_table = accel_profile.data(),
      .decel_table = decel_profile.data(),
      .accel_table_length =static_cast<uint8_t>(accel_profile.size()),
      .decel_table_length = static_cast<uint8_t>(decel_profile.size()),
  };

  SECTION("Empty Queue")
  {
    stepper_dir_t result = stepper_pop_next_step(cmd_queue_handle, &step_period);
    CHECK(result==STEPPER_DIR_STOP);
    CHECK(step_period==0);
  }

  SECTION("Coast")
  {
    stepper_dir_t direction = GENERATE(STEPPER_DIR_CW, STEPPER_DIR_CCW);
    CAPTURE(direction);

    stepper_cmd_t cmd = {
        .steps_remaining = 4,
        .mode = STEPPER_MODE_COAST,
        .dir = direction,
    };
    fast_cbuf_push(cmd_queue_handle, cmd);

    for (int i = 0; i < 5; ++i) {
      stepper_dir_t result = stepper_pop_next_step(cmd_queue_handle, &step_period);
      CHECK(result==direction);
      CHECK(step_period==0);
    }
    stepper_dir_t result = stepper_pop_next_step(cmd_queue_handle, &step_period);
    CHECK(result==STEPPER_DIR_STOP);
    CHECK(step_period==0);
    CHECK(fast_cbuf_empty(cmd_queue_handle));
  }

  SECTION("Accelerate")
  {
    stepper_dir_t direction = GENERATE(STEPPER_DIR_CW, STEPPER_DIR_CCW);
    CAPTURE(direction);

    stepper_cmd_t cmd = {
        .steps_remaining = 5,
        .step_profile = accel_profile.data(),
        .mode = STEPPER_MODE_ACCELERATE,
        .dir = direction,
    };
    stepper_queue_start(direction, cmd_queue_handle);
    fast_cbuf_push(cmd_queue_handle, cmd);

    for (int i = 0; i < 5; ++i) {
      stepper_dir_t result = stepper_pop_next_step(cmd_queue_handle, &step_period);
      CHECK(result==direction);
      CHECK(step_period==accel_profile.at(i));
    }

    stepper_dir_t result = stepper_pop_next_step(cmd_queue_handle, &step_period);
    CHECK(result==direction);
    result = stepper_pop_next_step(cmd_queue_handle, &step_period);
    CHECK(result==STEPPER_DIR_STOP);
    CHECK(fast_cbuf_empty(cmd_queue_handle));
  }
}

TEST_CASE("End Point Path Planning")
{
  std::vector<uint16_t> decel_profile{accel_profile};
  std::reverse(decel_profile.begin(), decel_profile.end());

  stepper_profile_t stepper_profile{
      .accel_table = accel_profile.data(),
      .decel_table = decel_profile.data(),
      .accel_table_length =static_cast<uint8_t>(accel_profile.size()),
      .decel_table_length = static_cast<uint8_t>(decel_profile.size()),
  };
  trapezoidal_path_t path_buffer[2];

  SECTION("Stopped at Target")
  {
    uint8_t target = GENERATE(1, 199, 50, 150);
    stepper_state_t initial_state{.abs_position=target, .step_period=0, .direction=STEPPER_DIR_STOP};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    CHECK(n==0);
  }

  SECTION("At Target with Initial Velocity")
  {
    uint8_t target = 0;
    stepper_state_t initial_state{.abs_position=target, .step_period=3459, .direction=STEPPER_DIR_CW};
    stepper_bounds_t bounds{target, target};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==2);

    CHECK(path_buffer[0].decel_steps==17);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 1);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[0].accel_steps==0);
    CHECK(path_buffer[0].coast_steps==0);
    CHECK(path_buffer[0].initial_delay_ms==0);

    CHECK(path_buffer[1].accel_steps==9);
    CHECK(path_buffer[1].accel_table==accel_profile.data());
    CHECK(path_buffer[1].coast_steps==0);
    CHECK(path_buffer[1].decel_steps==8);
    CHECK(path_buffer[1].decel_table==decel_profile.data() + 10);
    CHECK(path_buffer[1].direction==STEPPER_DIR_CCW);
    CHECK(path_buffer[1].initial_delay_ms==0);
  }

  SECTION("Overshooting Target")
  {
    uint8_t target = 10;
    stepper_state_t initial_state{.abs_position=0, .step_period=3459, .direction=STEPPER_DIR_CW};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==2);

    CHECK(path_buffer[0].decel_steps==17);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 1);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[0].accel_steps==0);
    CHECK(path_buffer[0].coast_steps==0);
    CHECK(path_buffer[0].initial_delay_ms==0);

    CHECK(path_buffer[1].accel_steps==4);
    CHECK(path_buffer[1].accel_table==accel_profile.data());
    CHECK(path_buffer[1].coast_steps==0);
    CHECK(path_buffer[1].decel_steps==3);
    CHECK(path_buffer[1].decel_table==decel_profile.data() + 15);
    CHECK(path_buffer[1].direction==STEPPER_DIR_CCW);
    CHECK(path_buffer[1].initial_delay_ms==0);
  }

  SECTION("Conserve Momentum")
  {
    uint8_t target = 90;
    stepper_state_t initial_state{.abs_position=0, .step_period=3683, .direction=STEPPER_DIR_CCW};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);

    // Excludes the final stop step
    CHECK(path_buffer[0].accel_steps==2);
    CHECK(path_buffer[0].accel_table==accel_profile.data() + 16);
    CHECK(path_buffer[0].coast_steps==91);
    CHECK(path_buffer[0].decel_steps==17);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 1);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CCW);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }

  SECTION("Long Path")
  {
    auto [start, target, dir] = GENERATE(std::tuple<uint8_t, uint8_t, stepper_dir_t>{0, 150, STEPPER_DIR_CCW},
                                         std::tuple<uint8_t, uint8_t, stepper_dir_t>{0, 50, STEPPER_DIR_CW},
                                         std::tuple<uint8_t, uint8_t, stepper_dir_t>{50, 0, STEPPER_DIR_CCW},
                                         std::tuple<uint8_t, uint8_t, stepper_dir_t>{150, 0, STEPPER_DIR_CW},
                                         std::tuple<uint8_t, uint8_t, stepper_dir_t>{150, 100, STEPPER_DIR_CCW},
                                         std::tuple<uint8_t, uint8_t, stepper_dir_t>{100, 150, STEPPER_DIR_CW});
    CAPTURE(start, target);
    stepper_state_t initial_state{.abs_position=start, .step_period=0, .direction=STEPPER_DIR_STOP};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);
    CHECK(path_buffer[0].accel_steps==18);
    CHECK(path_buffer[0].accel_table==accel_profile.data());
    CHECK(path_buffer[0].coast_steps==14);
    CHECK(path_buffer[0].decel_steps==17);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 1);
    CHECK(path_buffer[0].direction==dir);
    CHECK(path_buffer[0].duration_ms==276);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }

  SECTION("Short Path")
  {
    uint8_t target = 20;
    stepper_state_t initial_state{.abs_position=0, .step_period=0, .direction=STEPPER_DIR_STOP};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);
    CHECK(path_buffer[0].accel_steps==10);
    CHECK(path_buffer[0].accel_table==accel_profile.data());
    CHECK(path_buffer[0].coast_steps==0);
    CHECK(path_buffer[0].decel_steps==9);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 9);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[0].duration_ms==164);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }

  SECTION("Single Step")
  {
    uint8_t target = 1;
    stepper_state_t initial_state{.abs_position=0, .step_period=0, .direction=STEPPER_DIR_STOP};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);
    CHECK(path_buffer[0].accel_steps==0);
    CHECK(path_buffer[0].coast_steps==0);
    CHECK(path_buffer[0].decel_steps==0);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[0].duration_ms==0);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }

  SECTION("Double Step")
  {
    uint8_t target = 2;
    stepper_state_t initial_state{.abs_position=0, .step_period=0, .direction=STEPPER_DIR_STOP};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);
    CHECK(path_buffer[0].accel_steps==1);
    CHECK(path_buffer[0].accel_table==accel_profile.data());

    CHECK(path_buffer[0].coast_steps==0);
    CHECK(path_buffer[0].decel_steps==0);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[0].duration_ms==20);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }

  SECTION("Triple Step")
  {
    uint8_t target = 3;
    stepper_state_t initial_state{.abs_position=0, .step_period=0, .direction=STEPPER_DIR_STOP};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);
    CHECK(path_buffer[0].accel_steps==1);
    CHECK(path_buffer[0].accel_table==accel_profile.data());
    CHECK(path_buffer[0].coast_steps==1);
    CHECK(path_buffer[0].decel_steps==0);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[0].duration_ms==40);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }

  SECTION("With Initial Velocity")
  {
    uint8_t target = 50;
    stepper_state_t initial_state{.abs_position=0, .step_period=5374, .direction=STEPPER_DIR_CW};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);
    CHECK(path_buffer[0].accel_steps==10);
    CHECK(path_buffer[0].accel_table==accel_profile.data() + 8);
    CHECK(path_buffer[0].coast_steps==23);
    CHECK(path_buffer[0].decel_steps==17);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 1);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[0].duration_ms==233);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }

  SECTION("At Max Speed")
  {
    uint8_t target = 50;
    stepper_state_t initial_state{.abs_position=0, .step_period=3459, .direction=STEPPER_DIR_CW};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);

    CHECK(path_buffer[0].accel_steps==0);
    CHECK(path_buffer[0].coast_steps==33);
    CHECK(path_buffer[0].decel_steps==17);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 1);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }

  SECTION("Changing Direction")
  {
    uint8_t target = 50;
    stepper_state_t initial_state{.abs_position=0, .step_period=3459, .direction=STEPPER_DIR_CCW};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==2);
    CHECK(path_buffer[0].decel_steps==17);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 1);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CCW);
    CHECK(path_buffer[0].initial_delay_ms==0);

    CHECK(path_buffer[1].accel_steps==18);
    CHECK((int) path_buffer[1].coast_steps==32);
    CHECK(path_buffer[1].decel_steps==17);
    CHECK(path_buffer[1].decel_table==decel_profile.data() + 1);
    CHECK(path_buffer[1].direction==STEPPER_DIR_CW);
    CHECK(path_buffer[1].initial_delay_ms==0);
  }

  SECTION("Near Target")
  {
    uint8_t target = 0;
    stepper_state_t initial_state{.abs_position=5, .step_period=9545, .direction=STEPPER_DIR_CCW};
    uint8_t n = stepper_plan_endpoint_path(&stepper_profile,
                                           &initial_state,
                                           target,
                                           path_buffer);
    REQUIRE(n==1);

    CHECK(path_buffer[0].accel_steps==1);
    CHECK(path_buffer[0].coast_steps==1);
    CHECK(path_buffer[0].decel_steps==3);
    CHECK(path_buffer[0].decel_table==decel_profile.data() + 15);
    CHECK(path_buffer[0].direction==STEPPER_DIR_CCW);
    CHECK(path_buffer[0].initial_delay_ms==0);
  }
}

TEST_CASE("Find Future State") {
  uint8_t buffer[sizeof(stepper_cmd_queue_t) + 8*sizeof(stepper_cmd_t)];
  auto* cmd_queue_handle = static_cast<stepper_cmd_queue_t*>((void*) &buffer);
  cmd_queue_handle->read_idx = 0;
  cmd_queue_handle->write_idx = 0;
  cmd_queue_handle->size_mask = 0x7;
  uint16_t step_period = 0;

  std::vector<uint16_t> decel_profile{accel_profile};
  std::reverse(decel_profile.begin(), decel_profile.end());

  stepper_profile_t stepper_info{
      .accel_table = accel_profile.data(),
      .decel_table = decel_profile.data(),
      .accel_table_length =static_cast<uint8_t>(accel_profile.size()),
      .decel_table_length = static_cast<uint8_t>(decel_profile.size()),
  };

  SECTION("Stopped"){
    stepper_state_t initial_state{.abs_position=0, .step_period=0, .direction=STEPPER_DIR_STOP};
    stepper_state_t state = initial_state;
    CHECK(state.step_period==initial_state.step_period);
    CHECK(state.direction==initial_state.direction);
    CHECK(state.abs_position==initial_state.abs_position);
    stepper_find_future_state(cmd_queue_handle, &state, 100);
  }

  SECTION("Single Accel Cmd") {
    stepper_cmd_t cmd = {
        .steps_remaining = 18,
        .step_profile = accel_profile.data(),
        .mode = STEPPER_MODE_ACCELERATE,
        .dir = STEPPER_DIR_CW,
    };
    fast_cbuf_push(cmd_queue_handle, cmd);

    stepper_state_t initial_state{.abs_position=0, .step_period=0, .direction=STEPPER_DIR_STOP};
    stepper_state_t state = initial_state;

    stepper_find_future_state(cmd_queue_handle, &state, 0);

    CHECK(state.step_period==initial_state.step_period);
    CHECK(state.direction==initial_state.direction);
    CHECK(state.abs_position==initial_state.abs_position);

    state = initial_state;
    stepper_find_future_state(cmd_queue_handle, &state, 10);
    CHECK(state.step_period==20000);
    CHECK(state.direction==STEPPER_DIR_CW);
    CHECK(state.abs_position==1);

    state = initial_state;
    stepper_find_future_state(cmd_queue_handle, &state, 150);
    CHECK(state.step_period==3459);
    CHECK(state.direction==STEPPER_DIR_CW);
    CHECK(state.abs_position==18);
  }
}
