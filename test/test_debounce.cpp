#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include <util/debounce.h>

TEST_CASE("Button Debounce"){
  uint8_t pin = 0;
  debounced_button_t debounced_button{
    .pin = &pin,
    .idx = 0,
    .dc_mask = 0x00,
    .debounce_state = 0,
    .prev_state = BUTTON_LOW,
  };

  SECTION("Correct state (no bouncing)"){
    uint8_t idx = GENERATE(0, 1, 2, 3, 4, 5, 6, 7);
    uint8_t msk = GENERATE(0, 1, 2, 3, 4, 5, 6);
    CAPTURE(idx, msk);
    debounced_button.idx = idx;
    debounced_button.dc_mask = ((1 << msk) - 1) << (8-msk);
    pin = 1 << idx;
    for (int i = 0; i < (7-msk); ++i) {
      CAPTURE(i);
      CHECK(debounce_button(&debounced_button) == BUTTON_LOW);
    }
    CHECK(debounce_button(&debounced_button) == BUTTON_RISING);
    CHECK(debounce_button(&debounced_button) == BUTTON_HIGH);

    pin = 0;
    for (int i = 0; i < (7-msk); ++i) {
      CHECK(debounce_button(&debounced_button) == BUTTON_HIGH);
    }
    CHECK(debounce_button(&debounced_button) == BUTTON_FALLING);
    CHECK(debounce_button(&debounced_button) == BUTTON_LOW);
  }

  SECTION("Bouncing rejected"){
    uint8_t idx = GENERATE(0, 1, 2, 3, 4, 5, 6, 7);
    uint8_t msk = GENERATE(0, 1, 2, 3, 4, 5, 6);
    debounced_button.idx = idx;
    debounced_button.dc_mask = ((1 << msk) - 1) << (8-msk);
    pin = 1 << idx;
    for (int i = 0; i < 20; ++i) {
      CHECK(debounce_button(&debounced_button) == BUTTON_LOW);
      pin ^= 1 << idx;
    }
    for (int i = 0; i < (7-msk); ++i) {
      CHECK(debounce_button(&debounced_button) == BUTTON_LOW);
    }
    CHECK(debounce_button(&debounced_button) == BUTTON_RISING);
    CHECK(debounce_button(&debounced_button) == BUTTON_HIGH);

    pin = 0;
    for (int i = 0; i < 20; ++i) {
      CHECK(debounce_button(&debounced_button) == BUTTON_HIGH);
      pin ^= 1 << idx;
    }
    for (int i = 0; i < (7-msk); ++i) {
      CHECK(debounce_button(&debounced_button) == BUTTON_HIGH);
    }
    CHECK(debounce_button(&debounced_button) == BUTTON_FALLING);
    CHECK(debounce_button(&debounced_button) == BUTTON_LOW);
  }

}